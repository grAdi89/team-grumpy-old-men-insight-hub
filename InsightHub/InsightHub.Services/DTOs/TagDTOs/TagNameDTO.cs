﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.DTOs.TagDTOs
{
    /// <summary>
    /// A class that maps the Tag model to TagNameDTO with less properties for encapsulation purposes.
    /// </summary>
    public class TagNameDTO
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
    }
}
