﻿namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A class that maps the User model to UserDTO with less properties for encapsulation purposes.
    /// </summary>
    public class UserFullNameDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
