﻿using InsightHub.Services.ReportDTOs.DTOs;
using System.Collections.Generic;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A class that maps the User model to UserDTO with less properties for encapsulation purposes.
    /// </summary>
    public class UserDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsBanned { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public string Email { get; set; }
        public bool HasPendingReports { get; set; }        
    }
}
