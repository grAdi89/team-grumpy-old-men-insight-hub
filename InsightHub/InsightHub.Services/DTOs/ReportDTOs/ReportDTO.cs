﻿using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using System;
using System.Collections.Generic;

namespace InsightHub.Services.ReportDTOs.DTOs
{
    /// <summary>
    /// A class that maps the Report model to ReportCreateDTO with less properties for encapsulation purposes.
    /// </summary>
    public class ReportDTO
    {
        public ReportDTO()
        {
            this.ReportTags = new List<TagNameDTO>();
        }
        public Guid ReportID { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int AuthorID { get; set; }
        public UserDTO Author { get; set; }
        public int IndustryID { get; set; }
        public IndustryCreateDTO Industry { get; set; }
        public ICollection<TagNameDTO> ReportTags { get; set; }
        public string FileTitle { get; set; }
        public DateTime CreatedOn { get; set; }
        public int DownloadCount { get; set; }
        public bool IsPending { get; set; }
        public bool IsFeatured { get; set; }
    }
}
