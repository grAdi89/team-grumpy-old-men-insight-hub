﻿using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace InsightHub.Services.ReportDTOs.DTOs
{
    /// <summary>
    /// A class that maps the Report model to ReportCreateDTO wich has the needed properties for creating a Report.
    /// </summary>
    public class ReportCreateDTO
    {
        public ReportCreateDTO()
        {
            this.ReportTags = new List<TagNameDTO>();
        }
        public Guid ReportID { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public UserDTO Author { get; set; }
        public IndustryCreateDTO Industry { get; set; }
        public List<TagNameDTO> ReportTags { get; set; }
        public string FileTitle { get; set; }
        public IFormFile File { get; set; }
    }
}
