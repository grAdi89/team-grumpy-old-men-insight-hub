﻿using System;

namespace InsightHub.Services.ReportDTOs.DTOs
{
    /// <summary>
    /// A class that maps the Report model to ReportCreateDTO with less properties for encapsulation purposes.
    /// </summary>
    public class ReportNameDescrDTO
    {
        public Guid ReportID { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
    }
}
