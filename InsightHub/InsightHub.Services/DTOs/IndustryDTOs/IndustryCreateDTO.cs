﻿using InsightHub.Models;
using System.Collections.Generic;

namespace InsightHub.Services.IndustryDTOs.DTOs
{
    /// <summary>
    /// A class that maps the Industry model to IndustryCreateDTO wich has the needed properties for creating an Industry.
    /// </summary>
    public class IndustryCreateDTO
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
    }
}
