﻿using InsightHub.Models;
using InsightHub.Services.ReportDTOs.DTOs;
using System.Collections.Generic;

namespace InsightHub.Services.IndustryDTOs.DTOs
{
    /// <summary>
    /// A class that maps the Industry model to IndustryDTO with less properties for encapsulation purposes.
    /// </summary>
    public class IndustryDTO
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
        public ICollection<ReportNameDescrDTO> Reports { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
    }
}
