﻿using InsightHub.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A class that maps the Tag model to TagDTO with less properties for encapsulation purposes.
    /// </summary>
    public class TagDTO
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
        public ICollection<ReportTags> TagReports { get; set; }
    }
}
