﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Report model - 
    ///     - CreateReport
    ///      - DownloadReport
    ///       - GetMyDownloads
    ///        - GetMyReports
    ///         - GetReport
    ///          - GetReportForApproval
    ///           - GetAllReports
    ///            - FilterReports
    ///             - SortReports
    ///              - UpdateReport
    ///               - DeleteReport
    ///                - FeatureReport/UnfeatureReports
    ///                 - GetCurrentlyFeaturedReports
    /// </summary>
    public class ReportService : IReportService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly InsightHubContext _context;
        private readonly IBlobServices _blobService;
        private readonly ITagServices _tagServices;

        public ReportService(IDateTimeProvider dateTimeProvider, InsightHubContext context, IBlobServices blobService,
            ITagServices tagServices)
        {
            this._dateTimeProvider = dateTimeProvider;
            this._context = context;
            this._blobService = blobService;
            this._tagServices = tagServices;
        }
        public async Task<ReportCreateDTO> CreateReportAsync(ReportCreateDTO reportDTO)
        {
            if (reportDTO.File != null & reportDTO.Author != null & reportDTO.Description != null & reportDTO.Industry != null
                & reportDTO.ReportName != null & reportDTO.ReportTags.Count() > 0)
            {
                if (!await this._context.Reports.AnyAsync(x => x.ReportName == reportDTO.ReportName))
                {
                    var author = await _context.Users
                        .FirstOrDefaultAsync(x => x.Id == reportDTO.Author.UserID);
                    if (author == null) throw new ArgumentNullException("Author not found");

                    var industry = await _context.Industries
                        .Where(i => !i.IsDeleted)
                        .FirstOrDefaultAsync(x => x.IndustryID == reportDTO.Industry.IndustryID);
                    if (industry == null) throw new ArgumentNullException("Industry not found");

                    var report = new Report
                    {
                        ReportName = reportDTO.ReportName,
                        Description = reportDTO.Description,
                        AuthorID = reportDTO.Author.UserID,
                        Author = author,
                        IndustryID = reportDTO.Industry.IndustryID,
                        Industry = industry,
                        FileTitle = reportDTO.File.FileName,
                        IsPending = true
                    };


                    foreach (var tag in reportDTO.ReportTags)
                    {
                        var tagToAdd = await this._context.Tags
                            .FirstOrDefaultAsync(x => x.TagName == tag.TagName);

                        if (tagToAdd == null) throw new ArgumentNullException();

                        var reportTag = new ReportTags
                        {
                            TagID = tagToAdd.TagID,
                            Tag = tagToAdd,
                            Report = report,
                            ReportID = report.ReportID
                        };
                        report.ReportTags.Add(reportTag);

                        tag.TagName = tagToAdd.TagName;
                    }
                    author.Reports.Add(report);

                    report.CreatedOn = this._dateTimeProvider.GetDateTime();

                    await this._context.Reports.AddAsync(report);
                    await this._context.SaveChangesAsync();

                    await this._blobService.UploadFileAsync(reportDTO.File, report.ReportID.ToString());

                    reportDTO.FileTitle = report.FileTitle;
                    reportDTO.ReportID = report.ReportID;
                    reportDTO.Author = new UserDTO
                    { UserID = report.AuthorID, FirstName = report.Author.FirstName, LastName = report.Author.LastName };
                    reportDTO.Industry = new IndustryCreateDTO
                    { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName };
                    reportDTO.File = null;

                    return reportDTO;
                }

                throw new InvalidOperationException(nameof(ReportCreateDTO));
            }
            throw new InvalidOperationException($"No files attached");
        }

        public async Task<MemoryStream> DownloadReportAsync(Guid reportID, int userId)
        {
            var report = await this._context.Reports
                    .FirstOrDefaultAsync(r => r.ReportID == reportID);

            if (report == null)
            {
                throw new ArgumentNullException();
            }

            var user = await this._context.Users
                    .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var memory = await _blobService.DownloadFileAsync(report.ReportID.ToString(), report.FileTitle);

            if (!await this._context.DownloadedReports.AnyAsync(r => r.ReportID == reportID & r.UserID == userId))
            {
                var userDownload = new DownloadedReports
                {
                    Report = report,
                    ReportID = report.ReportID,
                    User = user,
                    UserID = user.Id
                };
                await _context.DownloadedReports.AddAsync(userDownload);
            }

            report.DownloadCount++;
            await _context.SaveChangesAsync();

            return memory;
        }

        public async Task<ICollection<DownloadedReports>> GetMyDownloadsAsync(int userID)
        {
            var downloads = await this._context.DownloadedReports
                .Include(r => r.Report)
                .Where(u => u.UserID == userID).ToListAsync();

            return downloads;
        }
        public async Task<ICollection<ReportDTO>> GetMyReportsAsync(int userID)
        {
            var myReports = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(tag => tag.Tag)
                .Where(r => !r.IsPending)
                .Where(u => u.AuthorID == userID).ToListAsync();

            var reportsDTO = myReports.Select(report => new ReportDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                AuthorID = report.AuthorID,
                Author = new UserDTO
                {
                    UserID = report.Author.Id,
                    FirstName = report.Author.FirstName,
                    LastName = report.Author.LastName,
                    Email = report.Author.Email
                },
                Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                ReportTags = report.ReportTags.Where(tag => !tag.Tag.IsDeleted).Select(tag => new TagNameDTO
                { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                FileTitle = report.FileTitle,
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount,
                IsFeatured = report.IsFeatured
            }).ToList();

            return reportsDTO;
        }

        public async Task<ReportDTO> GetReportAsync(Guid reportID)
        {
            var report = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(tag => tag.Tag)
                .FirstOrDefaultAsync(r => r.ReportID == reportID && !r.IsPending);

            if (report == null) throw new ArgumentNullException("Report not found");

            var reportDTO = new ReportDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                Author = new UserDTO
                { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName },
                Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                FileTitle = report.FileTitle,
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.Tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount
            };

            return reportDTO;
        }

        public async Task<ReportDTO> GetReportForApprovalAsync(Guid reportID)
        {
            var report = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(tag => tag.Tag)
                .FirstOrDefaultAsync(r => r.ReportID == reportID) ?? throw new ArgumentNullException("Report not found");           

            var reportDTO = new ReportDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                Author = new UserDTO
                { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName },
                Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                FileTitle = report.FileTitle,
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.Tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount
            };

            return reportDTO;
        }

        public async Task<ICollection<ReportDTO>> GetAllReportsAsync()
        {
            var reports = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(tag => tag.Tag)
                .Where(r => !r.IsPending).ToListAsync();

            var reportsDTO = reports.Select(report => new ReportDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                AuthorID = report.AuthorID,
                Author = new UserDTO
                {
                    UserID = report.Author.Id,
                    FirstName = report.Author.FirstName,
                    LastName = report.Author.LastName,
                    Email = report.Author.Email
                },
                Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                ReportTags = report.ReportTags.Where(tag => !tag.Tag.IsDeleted).Select(tag =>  new TagNameDTO
                { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount,
                IsFeatured = report.IsFeatured
            }).ToList();

            return reportsDTO;
        }

        public async Task<ICollection<ReportDTO>> FilterReportsAsync(string filterType, string filterName)
        {
            if (filterType.ToLower() == "industry")
            {
                var reports = await this._context.Reports
                    .Include(a => a.Author)
                    .Include(i => i.Industry)
                    .Include(rt => rt.ReportTags)
                    .ThenInclude(tag => tag.Tag)
                    .Where(r => r.Industry.IndustryName.ToLower() == filterName.ToLower())
                    .ToListAsync();

                if (reports.Count() > 0)
                {
                    return reports.Select(report => new ReportDTO
                    {
                        ReportID = report.ReportID,
                        ReportName = report.ReportName,
                        Description = report.Description,
                        Author = new UserDTO
                        { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName },
                        Industry = new IndustryCreateDTO
                        { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                        ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                        { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                        CreatedOn = report.CreatedOn,
                        DownloadCount = report.DownloadCount
                    }).ToList();
                }
                else
                {
                    throw new ArgumentNullException("No Industry with that name");
                }
            }
            else if (filterType.ToLower() == "tag")
            {
                var reportTags = await this._context.ReportTags
                    .Include(report => report.Report)
                    .Include(tag => tag.Tag)
                    .Where(t => t.Tag.TagName.ToLower() == filterName.ToLower())
                    .ToListAsync();

                if (reportTags.Count() > 0)
                {
                    ICollection<ReportDTO> reports = new List<ReportDTO>();

                    foreach (var reportTag in reportTags)
                    {
                        reports.Add(await this.GetReportAsync(reportTag.ReportID));
                    }

                    return reports;
                }
                else
                {
                    throw new ArgumentNullException("No Tag with that name");
                }
            }
            else if (filterType.ToLower() == "name")
            {
                var reports = await this._context.Reports
                   .Include(a => a.Author)
                   .Include(i => i.Industry)
                   .Include(rt => rt.ReportTags)
                   .ThenInclude(tag => tag.Tag)
                   .Where(r => r.ReportName.ToLower() == filterName.ToLower())
                   .ToListAsync();

                if (reports.Count() > 0)
                {
                    return reports.Select(report => new ReportDTO
                    {
                        ReportID = report.ReportID,
                        ReportName = report.ReportName,
                        Description = report.Description,
                        Author = new UserDTO
                        { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName },
                        Industry = new IndustryCreateDTO
                        { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                        ReportTags = report.ReportTags.Select(tag => new TagNameDTO { TagName = tag.Tag.TagName }).ToList(),
                        CreatedOn = report.CreatedOn,
                        DownloadCount = report.DownloadCount
                    }).ToList();
                }
                else
                {
                    throw new ArgumentNullException("No Report with that name");
                }
            }
            else
            {
                throw new ArgumentNullException("Filter type not supported");
            }
        }

        public async Task<ICollection<ReportDTO>> SortReportsAsync(string sortBy)
        {
            string filter = sortBy.ToLower();

            var allReports = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(tag => tag.Tag)
                .Where(r => !r.IsPending)
                .ToListAsync();

            IEnumerable<Report> sortedReports = new List<Report>();

            if (sortBy == "name") sortedReports = allReports.OrderBy(x => x.ReportName);
            if (sortBy == "downloads") sortedReports = allReports.OrderBy(x => x.DownloadCount);
            if (sortBy == "date") sortedReports = allReports.OrderBy(x => x.CreatedOn);

            var reportsDTO = sortedReports.Select(report => new ReportDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                Author = new UserDTO
                { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName },
                Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName },
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList(),
                FileTitle = report.FileTitle,
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount
            }).ToList();

            return reportsDTO;
        }

        public async Task<ReportDTO> UpdateReportAsync(ReportCreateDTO reportDTO)
        {
            var report = await this._context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(t => t.Tag)
                .FirstOrDefaultAsync(r => r.ReportID == reportDTO.ReportID);


            if (report == null) throw new ArgumentNullException("Report not found");

            var author = await _context.Users
                   .FirstOrDefaultAsync(x => x.Id == reportDTO.Author.UserID);

            var industry = await _context.Industries
                .Where(i => !i.IsDeleted)
                .FirstOrDefaultAsync(x => x.IndustryID == reportDTO.Industry.IndustryID);

            var updatedReportDTO = new ReportDTO
            {
                ReportID = report.ReportID,
                CreatedOn = report.CreatedOn,
                DownloadCount = report.DownloadCount
            };

            if (report.Author != author)
            {
                throw new ArgumentNullException("Author not found");
            }
            else
            {
                updatedReportDTO.Author = new UserDTO
                { UserID = report.Author.Id, FirstName = report.Author.FirstName, LastName = report.Author.LastName };
            }

            if (!string.IsNullOrWhiteSpace(reportDTO.ReportName) & reportDTO.ReportName != "string")
            {
                updatedReportDTO.ReportName = reportDTO.ReportName;
                report.ReportName = reportDTO.ReportName;
            }
            else
            {
                updatedReportDTO.ReportName = report.ReportName;
            }
            if (!string.IsNullOrWhiteSpace(reportDTO.Description) & reportDTO.Description != "string")
            {
                updatedReportDTO.Description = reportDTO.Description;
                report.Description = reportDTO.Description;
            }
            else
            {
                updatedReportDTO.Description = report.Description;
            }
            if (industry != null)
            {
                updatedReportDTO.Industry = new IndustryCreateDTO
                { IndustryID = industry.IndustryID, IndustryName = industry.IndustryName };
                report.IndustryID = industry.IndustryID;
                report.Industry = industry;
            }
            else
            {
                updatedReportDTO.Industry = new IndustryCreateDTO
                { IndustryID = report.IndustryID, IndustryName = report.Industry.IndustryName };
            }

            if (reportDTO.ReportTags.Count != 0 & !reportDTO.ReportTags.Any(t => t.TagID == 0)
                || !reportDTO.ReportTags.Any(t => t.TagName == null))
            {
                report.ReportTags.Clear();

                foreach (var tag in reportDTO.ReportTags)
                {
                    var tagToAdd = await _context.Tags
                        .Where(t => !t.IsDeleted)
                        .FirstOrDefaultAsync(x => x.TagName == tag.TagName);
                    if (tagToAdd == null) throw new ArgumentNullException($"Tag {tag.TagName} not found");

                    var reportTag = new ReportTags
                    {
                        TagID = tagToAdd.TagID,
                        Tag = tagToAdd,
                        Report = report,
                        ReportID = report.ReportID
                    };
                    report.ReportTags.Add(reportTag);
                }
            }

            updatedReportDTO.ReportTags = report.ReportTags
                .Select(reportTag => new TagNameDTO
                {
                    TagID = reportTag.TagID,
                    TagName = reportTag.Tag.TagName
                }).ToList();

            if (reportDTO.File != null)
            {
                await this._blobService.DeleteFileAsync(report.ReportID.ToString(), report.FileTitle);

                await this._blobService.UploadFileAsync(reportDTO.File, report.ReportID.ToString());

                report.FileTitle = reportDTO.File.FileName;

                updatedReportDTO.FileTitle = reportDTO.File.FileName;
            }
            else
            {
                updatedReportDTO.FileTitle = report.FileTitle;
            }

            await this._context.SaveChangesAsync();

            return updatedReportDTO;
        }

        public async Task<bool> DeleteReportAsync(Guid reportID)
        {
            var report = await this._context.Reports
                .FirstOrDefaultAsync(r => r.ReportID == reportID);

            if (report == null) return false;

            this._context.Reports.Remove(report);

            await this._blobService.DeleteFileAsync(report.ReportID.ToString(), report.FileTitle);

            await this._context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> FeatureReportAsync(Guid reportID)
        {
            try
            {
                var report = await _context.Reports
                    .FirstOrDefaultAsync(r => r.ReportID == reportID);

                if (report == null) return false;

                report.IsFeatured = true;
                report.FeaturedOn = _dateTimeProvider.GetDateTime();                

                await _context.SaveChangesAsync();
                await UnfeatureReports();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<IEnumerable<ReportDTO>> GetCurrentlyFeaturedReportsAsync()
        {
            var reports = await _context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .ThenInclude(t => t.Tag)
                .Where(r => r.IsFeatured)
                .Where(r => !r.IsPending)
                .ToListAsync();

            var reportsFeatured = reports
                .Select(r => new ReportDTO
                {
                    ReportID = r.ReportID,
                    ReportName = r.ReportName,
                    Description = r.Description,
                    Author = new UserDTO
                    { UserID = r.Author.Id, FirstName = r.Author.FirstName, LastName = r.Author.LastName },
                    Industry = new IndustryCreateDTO
                    { IndustryID = r.IndustryID, IndustryName = r.Industry.IndustryName },
                    IsFeatured = r.IsFeatured,
                    ReportTags = r.ReportTags.Select(rt => new TagNameDTO
                    {TagID = rt.TagID, TagName = rt.Tag.TagName }).ToList(),
                    CreatedOn = r.CreatedOn,
                    FileTitle = r.FileTitle
                });           

            return reportsFeatured;
        }

        private async Task<bool> UnfeatureReports()
        {
            var featuredReports = await _context.Reports
                .Where(r => r.IsFeatured)
                .OrderBy(r => r.FeaturedOn)                
                .ToListAsync();

            var reportsToBeUnfeatured = featuredReports.SkipLast(3).ToList();

            if (reportsToBeUnfeatured.Count == 0) return false;

            foreach (var rep in reportsToBeUnfeatured)
            {
                rep.IsFeatured = false;
            }

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
