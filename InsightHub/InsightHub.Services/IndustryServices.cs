﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Industry model - 
    ///  - GetIndustry
    ///    - GetAllIndustries
    ///     - CreateIndustry
    ///      - UpdateIndustry
    ///       - DeleteIndustry
    /// </summary>
    public class IndustryServices : IIndustryServices
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly InsightHubContext _context;

        public IndustryServices(IDateTimeProvider dateTimeProvider, InsightHubContext context)
        {
            _dateTimeProvider = dateTimeProvider;
            _context = context;
        }

        public async Task<IndustryDTO> GetIndustryAsync(int industryId)
        {
            var industry = await this._context.Industries
                .Include(r => r.Reports)
                .Include(sub => sub.SubscribedBy)
                .Where(d => !d.IsDeleted)
                .FirstOrDefaultAsync(x => x.IndustryID == industryId);

            if (industry == null) throw new ArgumentNullException(nameof(IndustryDTO));

            var industryDTO = new IndustryDTO
            {
                IndustryID = industry.IndustryID,
                IndustryName = industry.IndustryName,
                Reports = industry.Reports.Select(report => new ReportNameDescrDTO
                { ReportID = report.ReportID, ReportName = report.ReportName, Description = report.Description }).ToList(),
                Subscriptions = industry.SubscribedBy.Select(sub => new Subscription
                {
                    IndustryID = sub.IndustryID,
                    UserID = sub.UserID
                }).ToList()
            };

            return industryDTO;
        }

        public async Task<IEnumerable<IndustryCreateDTO>> GetAllIndustriesAsync()
        {
            var industries = await this._context.Industries
                .Include(sub => sub.SubscribedBy)
                .Where(industry => !industry.IsDeleted).ToListAsync();

            var industriesDTO = industries.Select(industry => new IndustryCreateDTO
            {
                IndustryID = industry.IndustryID,
                IndustryName = industry.IndustryName,
                Subscriptions = industry.SubscribedBy.Select(sub => new Subscription
                {
                    IndustryID = sub.IndustryID,
                    UserID = sub.UserID
                }).ToList()
            });

            return industriesDTO;
        }

        public async Task<IndustryCreateDTO> CreateIndustryAsync(string industryName)
        {
            if (!await this._context.Industries.AnyAsync(x => x.IndustryName == industryName && !x.IsDeleted))
            {
                var industry = new Industry
                {
                    IndustryName = industryName
                };

                industry.CreatedOn = this._dateTimeProvider.GetDateTime();
                await this._context.Industries.AddAsync(industry);
                await this._context.SaveChangesAsync();

                var industryDTO = new IndustryCreateDTO
                {
                    IndustryID = industry.IndustryID,
                    IndustryName = industryName
                };

                return industryDTO;
            }

            throw new InvalidOperationException(nameof(IndustryCreateDTO));
        }

        public async Task<IndustryCreateDTO> UpdateIndustryAsync(IndustryCreateDTO industryDTO)
        {
            var industry = await this._context.Industries
                .FirstOrDefaultAsync(x => x.IndustryID == industryDTO.IndustryID && !x.IsDeleted);

            if (industry != null)
            {
                industry.IndustryName = industryDTO.IndustryName;

                await this._context.SaveChangesAsync();

                return industryDTO;
            }

            throw new ArgumentNullException(nameof(IndustryCreateDTO));
        }

        public async Task<bool> DeleteIndustryAsync(int industryId)
        {
            var industry = await this._context.Industries
                .FirstOrDefaultAsync(industry => !industry.IsDeleted && industry.IndustryID == industryId);

            if (industry == null) throw new ArgumentNullException();

            industry.IsDeleted = true;
            industry.DeletedOn = this._dateTimeProvider.GetDateTime();
            await this._context.SaveChangesAsync();

            return true;
        }
    }
}
