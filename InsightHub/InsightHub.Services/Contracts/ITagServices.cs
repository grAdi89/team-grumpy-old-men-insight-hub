﻿using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Tag Services to access properties of the class - 
    ///     - GetTag
    ///      - GetAllTags
    ///       - CreateTag
    ///         - UpdateTag
    ///          - DeleteTag
    /// </summary>
    public interface ITagServices
    {
        Task<TagDTO> GetTagAsync(int tagId);
        Task<IEnumerable<TagDTO>> GetAllTagsAsync();
        Task<TagDTO> CreateTagAsync(string tagName);
        Task<TagDTO> UpdateTagAsync(TagDTO tagDTO);
        Task<bool> DeleteTagAsync(int tagId);
    }
}
