﻿using InsightHub.Services.DTOs;
using InsightHub.Services.ReportDTOs.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the User Service to access properties of the class - 
    ///     - GetUser
    ///      - GetAllUsers
    ///       - GetAllPendingApprovalUsers
    ///        - GetAllPendingApprovalReports
    ///         - DeleteUser
    ///          - BanUser/UnbanUser
    ///           - ApproveUser
    ///            - ApproveReport
    ///             - SubscribeToIndustry
    ///              - UnsubscribeFromIndustry                  
    /// </summary>
    public interface IUserService
    {
        Task<UserDTO> GetUserAsync(int id);
        Task<IEnumerable<UserDTO>> GetAllUsers();
        Task<IEnumerable<UserDTO>> GetAllPendingApprovalUsers();
        Task<IEnumerable<ReportDTO>> GetAllPendingApprovalReports();
        Task<bool> DeleteUserAsync(int id);        
        Task<bool> BanUserAsync(int id);
        Task<bool> UnbanUserAsync(int id);
        Task<bool> ApproveUserAsync(int id);
        Task<bool> ApproveReport(Guid id);
        Task<bool> SubscribeToIndustry(int industryID, int userID);
        Task<bool> UnsubscribeFromIndustry(int industryID, int userID);        
    }
}
