﻿using InsightHub.Models;
using InsightHub.Services.ReportDTOs.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Report Services to access properties of the class - 
    ///     - CreateReport
    ///      - DownloadReport
    ///       - GetMyDownloads
    ///        - GetMyReports
    ///         - GetReport
    ///          - GetReportForApproval
    ///           - GetAllReports
    ///            - FilterReports
    ///             - SortReports
    ///              - UpdateReport
    ///               - DeleteReport
    ///                - FeatureReport
    ///                 - GetCurrentlyFeaturedReports
    /// </summary>
    public interface IReportService
    {
        Task<ReportCreateDTO> CreateReportAsync(ReportCreateDTO reportDTO);
        Task<MemoryStream> DownloadReportAsync(Guid reportID, int userId);
        Task<ICollection<DownloadedReports>> GetMyDownloadsAsync(int userID);
        Task<ICollection<ReportDTO>> GetMyReportsAsync(int userID);
        Task<ReportDTO> GetReportAsync(Guid reportID);
        Task<ReportDTO> GetReportForApprovalAsync(Guid reportID);
        Task<ICollection<ReportDTO>> GetAllReportsAsync();
        Task<ICollection<ReportDTO>> FilterReportsAsync(string filterType, string filterName);
        Task<ICollection<ReportDTO>> SortReportsAsync(string sortBy);
        Task<ReportDTO> UpdateReportAsync(ReportCreateDTO reportDTO);
        Task<bool> DeleteReportAsync(Guid reportID);
        Task<bool> FeatureReportAsync(Guid reportID);
        Task<IEnumerable<ReportDTO>> GetCurrentlyFeaturedReportsAsync();
    }
}
