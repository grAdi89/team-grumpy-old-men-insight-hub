﻿using InsightHub.Services.DTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Industry Services to access properties of the class -
    ///  - GetIndustry
    ///    - GetAllIndustries
    ///     - CreateIndustry
    ///      - UpdateIndustry
    ///       - DeleteIndustry
    /// </summary>
    public interface IIndustryServices
    {
        Task<IndustryDTO> GetIndustryAsync(int industryId);
        Task<IEnumerable<IndustryCreateDTO>> GetAllIndustriesAsync();
        Task<IndustryCreateDTO> CreateIndustryAsync(string industryName);
        Task<IndustryCreateDTO> UpdateIndustryAsync(IndustryCreateDTO industryDTO);
        Task<bool> DeleteIndustryAsync(int industryId);
    }
}
