﻿using InsightHub.Data;
using InsightHub.EmailSender.Services;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Tag model - 
    ///     - DeleteUser
    ///      - GetUser
    ///       - GetAllUsers
    ///        - BanUser
    ///         - UnbanUser
    ///          - ApproveUser
    ///           - ApproveReport
    ///            - GetAllPendingApprovalUsers
    ///             - GetAllPendingApprovalReports
    ///              - SubscribeToIndustry
    ///               - UnsubscribeFromIndustry
    ///                - private GetAllUserEmailsSubscribedToIndustry
    ///                 - private SendEmailToSubscribers
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly InsightHubContext _context;
        private readonly IMyEmailSender _emailSender;

        public UserService(IDateTimeProvider dateTimeProvider, InsightHubContext context, IMyEmailSender emailSender)
        {
            _dateTimeProvider = dateTimeProvider;
            _context = context;
            _emailSender = emailSender;
        }      

        public async Task<bool> DeleteUserAsync(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(user => !user.IsDeleted && user.Id == id);

            if (user == null) return false;

            user.IsDeleted = true;
            user.DeleteOn = _dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<UserDTO> GetUserAsync(int id)
        {
            var user = await _context.Users
                .Include(user => user.Reports)
                .FirstOrDefaultAsync(user =>!user.IsDeleted && user.Id == id) ?? throw new ArgumentNullException(nameof(User));            

            var userDTO = new UserDTO
            {
                UserID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsApprovedByAdmin = user.IsApprovedByAdmin,
                HasPendingReports = user.Reports.Any(rep => rep.IsPending),
                IsBanned = user.IsBanned
            };

            return userDTO;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            var users = await _context.Users
                .Include(user => user.Reports)
                .Where(user => !user.IsDeleted)
                .Select(user => new UserDTO
            {
                UserID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsApprovedByAdmin = user.IsApprovedByAdmin,
                HasPendingReports = user.Reports.Any(rep => rep.IsPending),
                IsBanned = user.IsBanned
            }).ToListAsync();

            return users;
        }
        public async Task<bool> BanUserAsync(int id)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(user => user.IsApprovedByAdmin && !user.IsDeleted && !user.IsBanned && user.Id == id) ?? throw new ArgumentNullException(nameof(User));
            
            user.IsBanned = true;
            user.LockoutEnd = _dateTimeProvider.GetDateTime().AddYears(10);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UnbanUserAsync(int id)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(user => user.IsApprovedByAdmin && !user.IsDeleted && user.IsBanned && user.Id == id) ?? throw new ArgumentNullException(nameof(User));

            user.IsBanned = false;
            user.LockoutEnd = _dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ApproveUserAsync(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(user => !user.IsDeleted && user.Id == id);

            if (user == null) return false;

            user.IsApprovedByAdmin = true;
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ApproveReport(Guid id)
        {
            var report = await _context.Reports.FirstOrDefaultAsync(rep => rep.ReportID == id);

            if (report == null) return false;

            report.IsPending = false;

            await _context.SaveChangesAsync();

            var userEmails = await GetAllUserEmailsSubscribedToIndustry(report.IndustryID);
            if (userEmails.Any())
            {
                SendEmailToSubscribers(userEmails, report.ReportID);
            }
            return true;
        }

        public async Task<IEnumerable<UserDTO>> GetAllPendingApprovalUsers()
        {
            var usersPendingApproval = await _context.Users
                .Where(user => !user.IsDeleted && !user.IsApprovedByAdmin)
                .Select(user => new UserDTO
                {
                    UserID = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    IsBanned = user.IsBanned,
                    IsApprovedByAdmin = user.IsApprovedByAdmin
                }).ToListAsync();

            return usersPendingApproval;
        }

        public async Task<IEnumerable<ReportDTO>> GetAllPendingApprovalReports()
        {
            var reportsPendingApproval = await _context.Reports
                .Include(report => report.Author)
                .Where(report => report.IsPending)                
                .Select(report => new ReportDTO
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    AuthorID = report.AuthorID,
                    Author = new UserDTO { Email = report.Author.Email }
                }).ToListAsync();

            return reportsPendingApproval;
        }

        public async Task<bool> SubscribeToIndustry(int industryID, int userID)
        {
            var industry = await _context.Industries
                .FirstOrDefaultAsync(ind => !ind.IsDeleted && ind.IndustryID == industryID) ?? throw new ArgumentNullException(nameof(Industry));

            var user = await _context.Users
                .FirstOrDefaultAsync(user => !user.IsDeleted && user.Id == userID) ?? throw new ArgumentNullException(nameof(User));

            var sbscrp = await _context.Subscriptions
                .FirstOrDefaultAsync(sub => sub.UserID == user.Id && industryID == industry.IndustryID);

            if(sbscrp != null) throw new ArgumentException("Current user is already subscribed to this industry!");

            var subscription = new Subscription
            {
                IndustryID = industry.IndustryID,
                UserID = user.Id
            };

            await _context.Subscriptions.AddAsync(subscription);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UnsubscribeFromIndustry(int industryID, int userID)
        {
            var industry = await _context.Industries
                .FirstOrDefaultAsync(ind => !ind.IsDeleted && ind.IndustryID == industryID) ?? throw new ArgumentNullException(nameof(Industry));

            var user = await _context.Users
                .FirstOrDefaultAsync(user => !user.IsDeleted && user.Id == userID) ?? throw new ArgumentNullException(nameof(User));

            var sub = await _context.Subscriptions
                .FirstOrDefaultAsync(sub => sub.IndustryID == industryID && sub.UserID == userID) ?? throw new ArgumentNullException(nameof(Subscription));

            _context.Subscriptions.Remove(sub);
            await _context.SaveChangesAsync();

            return true;
        }

        private async Task<IEnumerable<string>> GetAllUserEmailsSubscribedToIndustry(int industryID)
        {
            var emails = await _context.Subscriptions
                .Include(u => u.User)
                .Where(s => s.IndustryID == industryID)
                ?.Select(e => e.User.Email)
                .ToListAsync();

            return emails;
        }
        private void SendEmailToSubscribers(IEnumerable<string> userEmails, Guid reportID)
        {
            var message = new Message(userEmails, "New report in your industry!", $"Link to the most recent report in your industry: " +
                $"http://localhost:60972/reports/details/{reportID}");

            _emailSender.SendEmailAsync(message);            
        }
    }
}
