﻿using InsightHub.Services.Providers.Contracts;
using System;

namespace InsightHub.Services.Providers
{
    /// <summary>
    /// A class that provides DateTime needed for CreatedOn, FeaturedOn.
    /// </summary>
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
