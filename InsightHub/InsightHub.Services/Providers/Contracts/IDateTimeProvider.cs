﻿using System;

namespace InsightHub.Services.Providers.Contracts
{
    /// <summary>
    /// Interface for the Date Time Provider to access property of the class - GetDateTime.
    /// </summary>
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
