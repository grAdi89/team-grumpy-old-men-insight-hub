﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.Providers.Contracts
{
    /// <summary>
    /// Interface for the File Type Provider to access property of the class - GetFileType.
    /// </summary>
    public interface IFileTypeProvider
    {
        string GetFileType(string fileName);
    }
}
