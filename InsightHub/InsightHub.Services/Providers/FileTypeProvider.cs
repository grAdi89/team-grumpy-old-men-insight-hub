﻿using InsightHub.Services.Providers.Contracts;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace InsightHub.Services.Providers
{
    /// <summary>
    /// A class that provides fileType needed for downloading a Report.
    /// </summary>
    public class FileTypeProvider : IFileTypeProvider
    {
        private readonly Dictionary<string, string> _extensionTypes = new Dictionary<string, string>
        {
                { ".jpg", "image/jpeg" },
                { ".jpeg", "image/jpeg" },
                { ".pdf", "application/pdf" }
        };

        public string GetFileType(string fileName)
        {
            var ext = Path.GetExtension(fileName).ToLowerInvariant();

            return _extensionTypes[ext];
        }
    }
}
