﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Tag model - 
    ///     - GetTag
    ///      - GetAllTags
    ///       - CreateTag
    ///         - UpdateTag
    ///          - DeleteTag
    /// </summary>
    public class TagServices : ITagServices
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly InsightHubContext _context;

        public TagServices(IDateTimeProvider dateTimeProvider, InsightHubContext context)
        {
            _dateTimeProvider = dateTimeProvider;
            _context = context;
        }

        public async Task<TagDTO> GetTagAsync(int tagId)
        {
            var tag = await this._context.Tags
                .Include(r => r.TagReports)
                .ThenInclude(report => report.Report)
                .Where(d => d.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.TagID == tagId);

            if (tag == null) throw new ArgumentNullException(nameof(TagDTO));

            var tagDTO = new TagDTO
            {
                TagID = tag.TagID,
                TagName = tag.TagName,
                TagReports = tag.TagReports
            };

            return tagDTO;
        }

        public async Task<IEnumerable<TagDTO>> GetAllTagsAsync()
        {
            var tags = await this._context.Tags
                .Include(rt => rt.TagReports)
                .ThenInclude(r => r.Report)
                .Where(tag => !tag.IsDeleted).ToListAsync();

            var tagsDTO = tags.Select(tag => new TagDTO
            {
                TagID = tag.TagID,
                TagName = tag.TagName,
                TagReports = tag.TagReports
            });

            return tagsDTO;
        }

        public async Task<TagDTO> CreateTagAsync(string tagName)
        {
            if (!await this._context.Tags.AnyAsync(x => x.TagName == tagName && x.IsDeleted == false))
            {
                var tag = new Tag
                {
                    TagName = tagName
                };

                tag.CreatedOn = this._dateTimeProvider.GetDateTime();
                await this._context.Tags.AddAsync(tag);
                await this._context.SaveChangesAsync();

                var tagDTO = new TagDTO
                {
                    TagID = tag.TagID,
                    TagName = tag.TagName
                };

                return tagDTO;
            }

            throw new InvalidOperationException(nameof(TagDTO));
        }

        public async Task<TagDTO> UpdateTagAsync(TagDTO tagDTO)
        {
            var tag = await this._context.Tags
                .FirstOrDefaultAsync(x => x.TagID == tagDTO.TagID && x.IsDeleted == false);

            if (tag != null)
            {
                if (!await this._context.Tags.AnyAsync(x => x.TagName == tagDTO.TagName && x.IsDeleted == false))
                {
                    tag.TagName = tagDTO.TagName;

                    await this._context.SaveChangesAsync();

                    return tagDTO;
                }
            }
            throw new ArgumentNullException();
        }

        public async Task<bool> DeleteTagAsync(int tagId)
        {
            var tag = await this._context.Tags
                .FirstOrDefaultAsync(tag => !tag.IsDeleted && tag.TagID == tagId);

            if (tag == null) throw new ArgumentNullException();

            tag.IsDeleted = true;
            tag.DeletedOn = this._dateTimeProvider.GetDateTime();
            await this._context.SaveChangesAsync();

            return true;
        }
    }
}
