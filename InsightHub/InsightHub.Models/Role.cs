﻿using Microsoft.AspNetCore.Identity;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of Identity Role entity.
    /// </summary>
    public class Role : IdentityRole<int>
    {
    }
}
