﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of User entity. It inherits Microsoft.AspNetCore.Identity.IdentityUser.
    /// </summary>
    public class User : IdentityUser<int>
    {
        public User()
        {
            this.Reports = new List<Report>();
            this.SubscribedTo = new List<Subscription>();
            this.ReportsDowloaded = new List<DownloadedReports>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Report> Reports { get; set; }
        public ICollection<Subscription> SubscribedTo { get; set; }        
        public ICollection<DownloadedReports> ReportsDowloaded { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public bool IsBanned { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeleteOn { get; set; }
    }
}
