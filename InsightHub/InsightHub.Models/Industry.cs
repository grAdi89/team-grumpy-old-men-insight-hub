﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of Industry entity.
    /// </summary>
    public class Industry
    {
        public Industry()
        {
            this.Reports = new List<Report>();
            this.SubscribedBy = new List<Subscription>();
        }
        public int IndustryID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string IndustryName { get; set; }
        public ICollection<Report> Reports { get; set; }
        public ICollection<Subscription> SubscribedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
