﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of Tag entity.
    /// </summary>
    public class Tag
    {
        public Tag()
        {
            this.TagReports = new List<ReportTags>();
        }
        public int TagID { get; set; }

        [StringLength(50, MinimumLength = 1)]
        public string TagName { get; set; }
        public ICollection<ReportTags> TagReports { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
