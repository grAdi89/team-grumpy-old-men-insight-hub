﻿using System;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of DownloadedReports entity.
    /// </summary>
    public class DownloadedReports
    {
        public int UserID { get; set; }
        public Guid ReportID { get; set; }
        public User User { get; set; }
        public Report Report { get; set; }
    }
}
