﻿using System;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of ReportTags entity.
    /// </summary>
    public class ReportTags
    {
        public int TagID { get; set; }
        public Guid ReportID { get; set; }
        public Report Report { get; set; }
        public Tag Tag { get; set; }
    }
}
