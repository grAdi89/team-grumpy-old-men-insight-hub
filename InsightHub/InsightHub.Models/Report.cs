﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of Report entity.
    /// </summary>
    public class Report
    {
        public Report()
        {
            this.ReportTags = new List<ReportTags>();
            this.DownloadedReport = new List<DownloadedReports>();
        }
        public Guid ReportID { get; set; }

        [StringLength(60, MinimumLength = 3)]
        public string ReportName { get; set; }

        [StringLength(250, MinimumLength = 10)]
        public string Description { get; set; }
        public int AuthorID { get; set; }
        public User Author { get; set; }
        public int IndustryID { get; set; }
        public Industry Industry { get; set; }
        public ICollection<ReportTags> ReportTags { get; set; }
        public ICollection<DownloadedReports> DownloadedReport { get; set; }

        [StringLength(65, MinimumLength = 3)]
        public string FileTitle { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime FeaturedOn { get; set; }
        public int DownloadCount { get; set; }
        public bool IsFeatured { get; set; }
        public bool IsPending { get; set; }
    }
}
