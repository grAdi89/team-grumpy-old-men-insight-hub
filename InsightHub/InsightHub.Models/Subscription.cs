﻿namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements the configuration properties of Subscription entity.
    /// </summary>
    public class Subscription
    {
        public int IndustryID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        public Industry Industry { get; set; }
    }
}
