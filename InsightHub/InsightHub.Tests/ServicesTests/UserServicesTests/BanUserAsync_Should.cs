﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class BanUserAsync_Should
    {
        [TestMethod]
        public async Task BanUser_Successfully()
        {
            var options = Utils.GetOptions(nameof(BanUser_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com"
            };

            var user = new User
            {
                Id = 2,
                FirstName = "Jack",
                LastName = "Sparrow",
                Email = "customer@customer.com",
                IsApprovedByAdmin = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(admin, user);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.BanUserAsync(2);                
                var bannedUser = await assertContext.Users.FirstOrDefaultAsync(user => user.Id == 2);

                Assert.AreEqual(true, result);                
                Assert.AreEqual(true, bannedUser.IsBanned);
            }
        }

        [TestMethod]
        public async Task BanUser_ThrowException_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(BanUser_ThrowException_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.BanUserAsync(1));
            }
        }
    }
}
