﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class UnbanUser_Should
    {
        //TODO
        //[TestMethod]
        //public async Task UnbanUser_Successfully()
        //{
        //    var options = Utils.GetOptions(nameof(UnbanUser_Successfully));
        //    var mockDateTimeProvider = new Mock<IDateTimeProvider>();
        //    var mockEmailSender = new Mock<IMyEmailSender>();

        //    var admin = new User
        //    {
        //        Id = 1,
        //        FirstName = "James",
        //        LastName = "Bond",
        //        Email = "admin@admin.com"
        //    };

        //    var user = new User
        //    {
        //        Id = 2,
        //        FirstName = "Jack",
        //        LastName = "Sparrow",
        //        Email = "customer@customer.com",
        //        IsBanned = true,
        //        LockoutEnabled = true,
        //        IsApprovedByAdmin = true
        //    };

        //    using (var arrangeContext = new InsightHubContext(options))
        //    {
        //        await arrangeContext.Users.AddRangeAsync(admin, user);
        //        await arrangeContext.SaveChangesAsync();
        //    }

        //    //Act & Assert
        //    using (var assertContext = new InsightHubContext(options))
        //    {
        //        var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
        //        var result = await sut.UnbanUserAsync(2);
        //        var unBannedUser = await sut.GetUserAsync(2);
        //        var usersCount = (await sut.GetAllUsers()).Count();

        //        Assert.AreEqual(true, result);
        //        Assert.AreEqual(2, usersCount);
        //        Assert.AreEqual(false, unBannedUser.IsBanned);
        //    }
        //}

        [TestMethod]
        public async Task UnbanUser_ThrowException_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(UnbanUser_ThrowException_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();           

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UnbanUserAsync(1));
            }
        }
    }
}
