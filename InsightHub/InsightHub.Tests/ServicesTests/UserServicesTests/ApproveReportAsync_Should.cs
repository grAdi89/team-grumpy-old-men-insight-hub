﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class ApproveReportAsync_Should
    {
        [TestMethod]
        public async Task ApproveReport_Successfully()
        {
            var options = Utils.GetOptions(nameof(ApproveReport_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var report = new Report { ReportName = "IT Report", IsPending = true };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.ApproveReport(report.ReportID);
                var reportsCount = (await assertContext.Reports.Where(rep => !rep.IsPending).ToListAsync()).Count();
                var approvedReport = await assertContext.Reports.FirstOrDefaultAsync();

                Assert.AreEqual(true, result);
                Assert.AreEqual(1, reportsCount);
                Assert.AreEqual(false, approvedReport.IsPending); 
            }
        }

        [TestMethod]
        public async Task ApproveReport_ReturnFalse_WhenReportNotFound()
        {
            var options = Utils.GetOptions(nameof(ApproveReport_ReturnFalse_WhenReportNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var id = new Guid();
                var result = await sut.ApproveReport(id);
                var expected = await assertContext.Reports.FirstOrDefaultAsync(rep => rep.ReportID == id);

                Assert.AreEqual(false, result);
                Assert.AreEqual(null, expected);
            }
        }
    }    
}
