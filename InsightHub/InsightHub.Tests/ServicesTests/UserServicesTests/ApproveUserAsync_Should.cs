﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class ApproveUserAsync_Should
    {
        [TestMethod]
        public async Task ApproveUser_Successfully()
        {
            var options = Utils.GetOptions(nameof(ApproveUser_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            var user = new User
            {
                Id = 2,
                FirstName = "Jack",
                LastName = "Sparrow",
                Email = "customer@customer.com",
                IsApprovedByAdmin = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(admin, user);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.ApproveUserAsync(2);
                var usersCount = (await assertContext.Users.Where(user => user.IsApprovedByAdmin).ToListAsync()).Count();
                var bannedUser = await assertContext.Users.FirstOrDefaultAsync(user => user.IsApprovedByAdmin && user.Id == 2);

                Assert.AreEqual(true, result);
                Assert.AreEqual(2, usersCount);
                Assert.AreEqual(true, bannedUser.IsApprovedByAdmin);
            }
        }

        [TestMethod]
        public async Task ApproveUser_ReturnFalse_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(ApproveUser_ReturnFalse_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.ApproveUserAsync(1);
                var expected = await assertContext.Users.FirstOrDefaultAsync(user => user.Id == 1 && !user.IsDeleted && user.IsApprovedByAdmin);

                Assert.AreEqual(false, result);
                Assert.AreEqual(null, expected);
            }
        }
    }
}
