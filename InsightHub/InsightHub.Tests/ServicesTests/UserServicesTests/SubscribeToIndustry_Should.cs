﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class SubscribeToIndustry_Should
    {
        [TestMethod]
        public async Task SubscribeToIndustry_Successfully()
        {
            var options = Utils.GetOptions(nameof(SubscribeToIndustry_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            var industry = new Industry { IndustryID = 1, IndustryName = "IT" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.SubscribeToIndustry(industry.IndustryID, admin.Id);
                var actualSubscription = await assertContext.Subscriptions.FirstOrDefaultAsync(sub => sub.IndustryID == industry.IndustryID && sub.UserID == admin.Id);

                Assert.AreEqual(true, result);

                Assert.AreEqual(actualSubscription.IndustryID, industry.IndustryID);
                Assert.AreEqual(actualSubscription.Industry.IndustryName, industry.IndustryName);

                Assert.AreEqual(actualSubscription.UserID, admin.Id);
                Assert.AreEqual(actualSubscription.User.Email, admin.Email);
            }
        }

        [TestMethod]
        public async Task SubscribeToIndustry_ThrowException_WhenIndustryNotFound()
        {
            var options = Utils.GetOptions(nameof(SubscribeToIndustry_ThrowException_WhenIndustryNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.SubscribeToIndustry(1, admin.Id));
            }
        }

        [TestMethod]
        public async Task SubscribeToIndustry_ThrowException_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(SubscribeToIndustry_ThrowException_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var industry = new Industry { IndustryID = 1, IndustryName = "IT" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.SubscribeToIndustry(industry.IndustryID, 1));
            }
        }

        [TestMethod]
        public async Task SubscribeToIndustry_ThrowException_WhenSubscriptionExists()
        {
            var options = Utils.GetOptions(nameof(SubscribeToIndustry_ThrowException_WhenSubscriptionExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            var industry = new Industry { IndustryID = 1, IndustryName = "IT" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.Subscriptions.AddAsync(new Subscription { IndustryID = industry.IndustryID, UserID = admin.Id });
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.SubscribeToIndustry(industry.IndustryID, admin.Id));
            }
        }
    }
}    
  

