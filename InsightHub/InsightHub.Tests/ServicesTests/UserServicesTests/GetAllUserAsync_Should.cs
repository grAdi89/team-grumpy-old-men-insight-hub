﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using System.Linq;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class GetAllUserAsync_Should
    {
        [TestMethod]
        public async Task GetAllUsers_Successfully()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com"
            };

            var author = new User
            {
                Id = 2,
                FirstName = "Obi-Wan",
                LastName = "Kenobi",
                Email = "author@author.com"
            };

            var customer = new User
            {
                Id = 3,
                FirstName = "Dart",
                LastName = "Vader",
                Email = "customer@customer.com"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(admin, author, customer);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = (await sut.GetAllUsers()).ToList();

                Assert.AreEqual(admin.Id, result[0].UserID);
                Assert.AreEqual(admin.Email, result[0].Email);

                Assert.AreEqual(author.Id, result[1].UserID);
                Assert.AreEqual(author.Email, result[1].Email);

                Assert.AreEqual(customer.Id, result[2].UserID);
                Assert.AreEqual(customer.Email, result[2].Email);
            }
        }

        [TestMethod]
        public async Task GetAllUsers_Successfully_WhenUserIsDeleted()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Successfully_WhenUserIsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com"
            };

            var author = new User
            {
                Id = 2,
                FirstName = "Obi-Wan",
                LastName = "Kenobi",
                Email = "author@author.com"
            };

            var customer = new User
            {
                Id = 3,
                FirstName = "Dart",
                LastName = "Vader",
                Email = "customer@customer.com"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(admin, author, customer);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                await sut.DeleteUserAsync(3);
                var result = (await sut.GetAllUsers()).ToList();                

                Assert.AreEqual(admin.Id, result[0].UserID);
                Assert.AreEqual(admin.Email, result[0].Email);

                Assert.AreEqual(author.Id, result[1].UserID);
                Assert.AreEqual(author.Email, result[1].Email);

                Assert.AreEqual(2, result.Count);
            }
        }
    }
}
