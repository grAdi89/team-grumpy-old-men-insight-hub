﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class DeleteUserAsync_Should
    {
        [TestMethod]
        public async Task DeleteUser_Successfully()
        {
            var options = Utils.GetOptions(nameof(DeleteUser_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com"                
            };           

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.DeleteUserAsync(1);
                var expected = await assertContext.Users.FirstOrDefaultAsync(user => user.Id == admin.Id && !user.IsDeleted);

                Assert.AreEqual(true, result);
                Assert.AreEqual(null, expected);
            }
        }

        [TestMethod]
        public async Task DeleteUser_ReturnFalse_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(DeleteUser_ReturnFalse_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.DeleteUserAsync(1);
                var expected = await assertContext.Users.FirstOrDefaultAsync(user => user.Id == 1 && !user.IsDeleted);

                Assert.AreEqual(false, result);
                Assert.AreEqual(null, expected);
            }
        }
    }    
}
