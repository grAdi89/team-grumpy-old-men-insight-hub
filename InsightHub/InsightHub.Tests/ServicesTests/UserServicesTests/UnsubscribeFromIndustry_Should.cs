﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class UnsubscribeFromIndustry_Should
    {
        [TestMethod]
        public async Task UnsubscribeFromIndustry_Successfully()
        {
            var options = Utils.GetOptions(nameof(UnsubscribeFromIndustry_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            var industry = new Industry { IndustryID = 1, IndustryName = "IT" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.Industries.AddAsync(industry);

                var subscription = new Subscription { IndustryID = industry.IndustryID, UserID = admin.Id };

                await arrangeContext.Subscriptions.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.UnsubscribeFromIndustry(industry.IndustryID, admin.Id);
                var actualSubscription = await assertContext.Subscriptions.FirstOrDefaultAsync(sub => sub.IndustryID == industry.IndustryID && sub.UserID == admin.Id);

                Assert.AreEqual(true, result);
                Assert.AreEqual(null, actualSubscription);
            }
        }

        [TestMethod]
        public async Task UnsubscribeFromIndustry_ThrowException_WhenSubscriptionIsNotFound()
        {
            var options = Utils.GetOptions(nameof(UnsubscribeFromIndustry_ThrowException_WhenSubscriptionIsNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UnsubscribeFromIndustry(1, 1));
            }
        }

        [TestMethod]
        public async Task UnsubscribeFromIndustry_ThrowException_WhenIndustryIsNotFound()
        {
            var options = Utils.GetOptions(nameof(UnsubscribeFromIndustry_ThrowException_WhenIndustryIsNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UnsubscribeFromIndustry(1, admin.Id));
            }
        }

        [TestMethod]
        public async Task UnsubscribeFromIndustry_ThrowException_WhenUserIsNotFound()
        {
            var options = Utils.GetOptions(nameof(UnsubscribeFromIndustry_ThrowException_WhenUserIsNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var industry = new Industry { IndustryID = 1, IndustryName = "IT" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UnsubscribeFromIndustry(industry.IndustryID, 1));
            }
        }
    }
}
