﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class GetAllPendingApprovalUsers_Should
    {
        [TestMethod]
        public async Task GetAllPendingApprovalUsers_Successfully()
        {
            var options = Utils.GetOptions(nameof(GetAllPendingApprovalUsers_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                IsApprovedByAdmin = true
            };

            var user = new User
            {
                Id = 2,
                FirstName = "Jack",
                LastName = "Sparrow",
                Email = "customer@customer.com",
                IsApprovedByAdmin = false
            };

            var secondUser = new User
            {
                Id = 3,
                FirstName = "Captain",
                LastName = "Barbosa",
                Email = "customer2@customer.com",
                IsApprovedByAdmin = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(admin, user, secondUser);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var pendingUsers = (await sut.GetAllPendingApprovalUsers()).ToList();
                var expectedUsersCount = 2;

                Assert.AreEqual(expectedUsersCount, pendingUsers.Count);

                Assert.AreEqual(user.Id, pendingUsers[0].UserID);
                Assert.AreEqual(user.Email, pendingUsers[0].Email);

                Assert.AreEqual(secondUser.Id, pendingUsers[1].UserID);
                Assert.AreEqual(secondUser.Email, pendingUsers[1].Email);
            }
        }
    }
}
