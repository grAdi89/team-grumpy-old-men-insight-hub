﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class GetAllPendingApprovalReports_Should
    {
        [TestMethod]
        public async Task GetAllPendingApprovalReports_Successfully()
        {
            var options = Utils.GetOptions(nameof(GetAllPendingApprovalReports_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var author = new User { Id = 1, Email = "author@author.com" };

            var itReport = new Report { ReportName = "IT", AuthorID = 1, Author = author, IsPending = false };
            var businessReport = new Report { ReportName = "Business", AuthorID = 1, Author = author, IsPending = true };
            var foodReport = new Report { ReportName = "Food", AuthorID = 1, Author = author, IsPending = true };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddRangeAsync(itReport, businessReport, foodReport);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var pendingReports = (await sut.GetAllPendingApprovalReports()).ToList();
                var expectedReportsCount = 2;

                Assert.AreEqual(expectedReportsCount, pendingReports.Count);

                Assert.AreEqual(businessReport.ReportID, pendingReports[0].ReportID);
                Assert.AreEqual(businessReport.ReportName, pendingReports[0].ReportName);

                Assert.AreEqual(foodReport.ReportID, pendingReports[1].ReportID);
                Assert.AreEqual(foodReport.ReportName, pendingReports[1].ReportName);
            }
        }
    }
}
