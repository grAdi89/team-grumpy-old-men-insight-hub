﻿using InsightHub.Data;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using InsightHub.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class GetUserAsync_Should
    {
        [TestMethod]
        public async Task GetUser_Succesfully()
        {
            var options = Utils.GetOptions(nameof(GetUser_Succesfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com"               
            };            

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(admin);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);
                var result = await sut.GetUserAsync(1);
                var expected = new UserDTO { UserID = 1, FirstName = "James", LastName = "Bond", Email = "admin@admin.com" };

                Assert.AreEqual(result.UserID, expected.UserID);
                Assert.AreEqual(result.FirstName, expected.FirstName);
                Assert.AreEqual(result.LastName, expected.LastName);
                Assert.AreEqual(result.Email, expected.Email);
            }
        }

        [TestMethod]
        public async Task GetUser_ThrowException_WhenUserNotFound()
        {
            var options = Utils.GetOptions(nameof(GetUser_ThrowException_WhenUserNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockEmailSender = new Mock<IMyEmailSender>();

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(mockDateTimeProvider.Object, assertContext, mockEmailSender.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserAsync(1));
            }
        }
    }
}
