﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.Providers.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class SortReportsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectReports_WhenSortReports_ByName_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectReports_WhenSortReports_ByName_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var firstIndustry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var secondIndustry = new Industry
            {
                IndustryName = "Banking"
            };
            var firstTag = new Tag
            {
                TagName = "Security & Risk"
            };
            var secondTag = new Tag
            {
                TagName = "Employee Experience"
            };
            var firstReportTag = new ReportTags
            {
                Tag = firstTag
            };
            var secondReportTag = new ReportTags
            {
                Tag = secondTag
            };
            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var firstReport = new Report
            {
                ReportName = "Winning The New B2B Buyer",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = firstIndustry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            var secondReport = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = user,
                Industry = secondIndustry,
                FileTitle = "Winning The New B2B Buyer.pdf"
            };
            user.Reports.Add(firstReport);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddRangeAsync(firstIndustry, secondIndustry);
                firstReportTag.TagID = firstTag.TagID;
                secondReportTag.TagID = secondTag.TagID;
                await arrangeContext.ReportTags.AddRangeAsync(firstReportTag, secondReportTag);
                firstReport.ReportTags.Add(firstReportTag);
                secondReport.ReportTags.Add(secondReportTag);
                await arrangeContext.Reports.AddRangeAsync(firstReport, secondReport);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                var result = await sut.SortReportsAsync("name");
                int expectedCount = 2;

                Assert.AreEqual(secondReport.ReportName, result.ElementAt(0).ReportName);
                Assert.AreEqual(firstReport.ReportName, result.ElementAt(1).ReportName);
                Assert.AreEqual(expectedCount, result.Count());
            }
        }
        [TestMethod]
        public async Task ReturnCorrectReports_WhenSortReports_ByDownloads_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectReports_WhenSortReports_ByDownloads_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var firstIndustry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var secondIndustry = new Industry
            {
                IndustryName = "Banking"
            };
            var firstTag = new Tag
            {
                TagName = "Security & Risk"
            };
            var secondTag = new Tag
            {
                TagName = "Employee Experience"
            };
            var firstReportTag = new ReportTags
            {
                Tag = firstTag
            };
            var secondReportTag = new ReportTags
            {
                Tag = secondTag
            };
            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var firstReport = new Report
            {
                ReportName = "Winning The New B2B Buyer",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = firstIndustry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf",
                DownloadCount = 2
            };
            var secondReport = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = user,
                Industry = secondIndustry,
                FileTitle = "Winning The New B2B Buyer.pdf",
                DownloadCount = 5
            };
            user.Reports.Add(firstReport);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddRangeAsync(firstIndustry, secondIndustry);
                firstReportTag.TagID = firstTag.TagID;
                secondReportTag.TagID = secondTag.TagID;
                await arrangeContext.ReportTags.AddRangeAsync(firstReportTag, secondReportTag);
                firstReport.ReportTags.Add(firstReportTag);
                secondReport.ReportTags.Add(secondReportTag);
                await arrangeContext.Reports.AddRangeAsync(firstReport, secondReport);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                var result = await sut.SortReportsAsync("name");
                int expectedCount = 2;

                Assert.AreEqual(secondReport.ReportName, result.ElementAt(0).ReportName);
                Assert.AreEqual(firstReport.ReportName, result.ElementAt(1).ReportName);
                Assert.AreEqual(expectedCount, result.Count());
            }
        }
        [TestMethod]
        public async Task ReturnCorrectReports_WhenSortReports_ByDate_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectReports_WhenSortReports_ByDate_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var firstIndustry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var secondIndustry = new Industry
            {
                IndustryName = "Banking"
            };
            var firstTag = new Tag
            {
                TagName = "Security & Risk"
            };
            var secondTag = new Tag
            {
                TagName = "Employee Experience"
            };
            var firstReportTag = new ReportTags
            {
                Tag = firstTag
            };
            var secondReportTag = new ReportTags
            {
                Tag = secondTag
            };
            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var firstReport = new Report
            {
                ReportName = "Winning The New B2B Buyer",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = firstIndustry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            var secondReport = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = user,
                Industry = secondIndustry,
                FileTitle = "Winning The New B2B Buyer.pdf",
                CreatedOn = DateTime.Now
            };
            user.Reports.Add(firstReport);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddRangeAsync(firstIndustry, secondIndustry);
                firstReportTag.TagID = firstTag.TagID;
                secondReportTag.TagID = secondTag.TagID;
                await arrangeContext.ReportTags.AddRangeAsync(firstReportTag, secondReportTag);
                firstReport.ReportTags.Add(firstReportTag);
                secondReport.ReportTags.Add(secondReportTag);
                await arrangeContext.Reports.AddRangeAsync(firstReport, secondReport);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                var result = await sut.SortReportsAsync("name");
                int expectedCount = 2;

                Assert.AreEqual(secondReport.ReportName, result.ElementAt(0).ReportName);
                Assert.AreEqual(firstReport.ReportName, result.ElementAt(1).ReportName);
                Assert.AreEqual(expectedCount, result.Count());
            }
        }
    }
}
