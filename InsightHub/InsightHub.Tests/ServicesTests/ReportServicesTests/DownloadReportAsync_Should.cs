﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class DownloadReportAsync_Should
    {
        [TestMethod]
        public async Task ThrowException_WhenDownload_ReportDoesNotExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenDownload_ReportDoesNotExist));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new ReportCreateDTO
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = new UserDTO
                { UserID = 1 },
                Industry = new IndustryCreateDTO { IndustryID = 1 },
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf",
                File = file
            };
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags = new List<TagNameDTO>()
                { new TagNameDTO { TagID = reportTag.TagID, TagName = reportTag.Tag.TagName } };
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadReportAsync(report.ReportID, user.Id));
            }
        }
        [TestMethod]
        public async Task ThrowException_WhenDownload_UserDoesNotExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenDownload_UserDoesNotExist));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new ReportCreateDTO
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = new UserDTO
                { UserID = 1 },
                Industry = new IndustryCreateDTO { IndustryID = 1 },
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf",
                File = file
            };
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags = new List<TagNameDTO>()
                { new TagNameDTO { TagID = reportTag.TagID, TagName = reportTag.Tag.TagName } };
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                var createdReport = await sut.CreateReportAsync(report);
                int notExistingUserId = 2;

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadReportAsync(createdReport.ReportID, notExistingUserId));
            }
        }
    }
}
