﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class DeleteReportAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_WhenDeleteReport_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_WhenDeleteReport_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                bool result = await sut.DeleteReportAsync(report.ReportID);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task RemoveFromDatabase_WhenDeleteReport_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(RemoveFromDatabase_WhenDeleteReport_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                bool result = await sut.DeleteReportAsync(report.ReportID);
                var expected = await assertContext.Reports.FirstOrDefaultAsync(n => n.ReportID == report.ReportID);

                Assert.IsNull(expected);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_WhenDeleteReport_DoesNotExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_WhenDeleteReport_DoesNotExist));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();


            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                var id = new Guid();
                bool result = await sut.DeleteReportAsync(id);

                Assert.IsFalse(result);
            }
        }
    }
}
