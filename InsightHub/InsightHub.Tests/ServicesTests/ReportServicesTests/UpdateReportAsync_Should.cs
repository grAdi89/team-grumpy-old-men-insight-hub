﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class UpdateReportAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectReport_WhenUpdateReport_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectReport_WhenUpdateReport_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var secondIndustry = new Industry
            {
                IndustryName = "Banking"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var secondTag = new Tag
            {
                TagName = "Employee Experience"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };
            var secondReportTag = new ReportTags
            {
                Tag = secondTag
            };
            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(tag, secondTag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddRangeAsync(industry, secondIndustry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddRangeAsync(reportTag, secondReportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            var updateReport = new ReportCreateDTO
            {
                ReportID = report.ReportID,
                ReportName = "The Future Of Financial Brands",
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = new UserDTO
                { UserID = report.AuthorID },
                Industry = new IndustryCreateDTO { IndustryID = secondIndustry.IndustryID },
                FileTitle = report.FileTitle,
                ReportTags = new List<TagNameDTO>()
                { new TagNameDTO { TagID = secondReportTag.TagID, TagName = secondReportTag.Tag.TagName } }
            };

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);

                var result = await sut.UpdateReportAsync(updateReport);

                Assert.AreEqual(updateReport.ReportName, result.ReportName);
                Assert.AreEqual(updateReport.Description, result.Description);
                Assert.AreEqual(updateReport.Author.UserID, result.Author.UserID);
                Assert.AreEqual(updateReport.Industry.IndustryID, result.Industry.IndustryID);
                Assert.AreEqual(updateReport.ReportTags.ElementAt(0).TagID, result.ReportTags.ElementAt(0).TagID);
                Assert.AreEqual(updateReport.FileTitle, result.FileTitle);
            }
        }
        [TestMethod]
        public async Task ThrowException_WhenUpdate_Report_ParamsInValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUpdate_Report_ParamsInValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            var updateReport = new ReportCreateDTO
            {
                ReportID = new Guid(),
                ReportName = report.ReportName,
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = new UserDTO
                { UserID = report.AuthorID },
                Industry = new IndustryCreateDTO { IndustryID = report.IndustryID },
                FileTitle = report.FileTitle,
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList()
            };

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateReportAsync(updateReport));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenUpdate_Author_ParamsInValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUpdate_Author_ParamsInValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            var updateReport = new ReportCreateDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = new UserDTO
                { UserID = 2 },
                Industry = new IndustryCreateDTO { IndustryID = report.IndustryID },
                FileTitle = report.FileTitle,
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.TagID, TagName = tag.Tag.TagName }).ToList()
            };

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateReportAsync(updateReport));
            }
        }
        [TestMethod]
        public async Task ThrowException_WhenUpdate_Tag_ParamsInValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUpdate_Tag_ParamsInValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };

            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };

            var report = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf"
            };
            user.Reports.Add(report);

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                report.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            var updateReport = new ReportCreateDTO
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = new UserDTO
                { UserID = report.AuthorID },
                Industry = new IndustryCreateDTO { IndustryID = report.IndustryID },
                FileTitle = report.FileTitle,
                ReportTags = report.ReportTags.Select(tag => new TagNameDTO
                { TagID = tag.TagID, TagName = "wrongName" }).ToList()
            };

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateReportAsync(updateReport));
            }
        }
    }
}
