﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.Providers.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class UnfeatureReports_Should
    {
        [TestMethod]
        public async Task UnfeatureReports_WhenFeatureReports_MoreThenThree()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UnfeatureReports_WhenFeatureReports_MoreThenThree));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockBlobServices = new Mock<IBlobServices>();
            var mockTagServices = new Mock<ITagServices>();
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "Revitalize CX Measurement & Prioritization.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            var file = fileMock.Object;

            var industry = new Industry
            {
                IndustryName = "Wealth Management"
            };
            var tag = new Tag
            {
                TagName = "Security & Risk"
            };
            var reportTag = new ReportTags
            {
                Tag = tag
            };
            var user = new User
            {
                FirstName = "Jean",
                LastName = "Birch",
                Email = "Jean@Birch.com",
                PhoneNumber = "+99999999999",
                PasswordHash = "123Jean@"
            };


            DateTime firstDateTime = new DateTime(2020, 5, 28, 10, 46, 33);
            DateTime secondDateTime = new DateTime(2020, 6, 28, 10, 46, 33);
            DateTime thirdDateTime = new DateTime(2020, 7, 28, 10, 46, 33);
            mockDateTimeProvider.Setup(tp => tp.GetDateTime()).Returns(new DateTime(2020, 8, 28));

            var firstReport = new Report
            {
                ReportName = "Revitalize CX Measurement & Prioritization",
                Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                Author = user,
                Industry = industry,
                FileTitle = "Revitalize CX Measurement & Prioritization.pdf",
                IsFeatured = true,
                FeaturedOn = firstDateTime
            };
            var secondReport = new Report
            {
                ReportName = "Winning The New B2B Buyer",
                Description = "Deliver Experiences That Are Open, Connected, And Intuitive",
                Author = user,
                Industry = industry,
                FileTitle = "Winning The New B2B Buyer.pdf",
                IsFeatured = true,
                FeaturedOn = secondDateTime
            };
            var thirdReport = new Report
            {
                ReportName = "How To Prove The ROI Of CX",
                Description = "You know that customer experience (CX) is a win-win for your firm and its customers",
                Author = user,
                Industry = industry,
                FileTitle = "How To Prove The ROI Of CX.pdf",
                IsFeatured = true,
                FeaturedOn = thirdDateTime
            };
            var fourthReport = new Report
            {
                ReportName = "Reducing Software Costs In 2020",
                Description = "Learn negotiation tactics to find short term software cost savings for both existing contracts and upcoming renewals.",
                Author = user,
                Industry = industry,
                FileTitle = "Reducing Software Costs In 2020.pdf",
                FeaturedOn = DateTime.Now
            };
            user.Reports.Add(firstReport);
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Industries.AddAsync(industry);
                reportTag.TagID = tag.TagID;
                await arrangeContext.ReportTags.AddAsync(reportTag);
                firstReport.ReportTags.Add(reportTag);
                secondReport.ReportTags.Add(reportTag);
                await arrangeContext.Reports.AddRangeAsync(firstReport, secondReport, thirdReport, fourthReport);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                await sut.FeatureReportAsync(fourthReport.ReportID);
                var result = await sut.GetCurrentlyFeaturedReportsAsync();
                int expectedCount = 3;

                Assert.AreEqual(secondReport.ReportName, result.ElementAt(0).ReportName);
                Assert.AreEqual(thirdReport.ReportName, result.ElementAt(1).ReportName);
                Assert.AreEqual(fourthReport.ReportName, result.ElementAt(2).ReportName);
                Assert.AreEqual(expectedCount, result.Count());
            }
        }
    }
}
