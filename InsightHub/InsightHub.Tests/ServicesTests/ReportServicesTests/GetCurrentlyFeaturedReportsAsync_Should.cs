﻿using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.Providers.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.ReportServicesTests
{
    [TestClass]
    public class GetCurrentlyFeaturedReportsAsync_Should
    {
        [TestClass]
        public class FeatureReportAsync_Should
        {
            [TestMethod]
            public async Task ReturnCorrect_WhenFeatureReport_ParamsAreValid()
            {
                //Arrange
                var options = Utils.GetOptions(nameof(ReturnCorrect_WhenFeatureReport_ParamsAreValid));
                var mockDateTimeProvider = new Mock<IDateTimeProvider>();
                var mockBlobServices = new Mock<IBlobServices>();
                var mockTagServices = new Mock<ITagServices>();
                var fileMock = new Mock<IFormFile>();
                var content = "Hello World from a Fake File";
                var fileName = "Revitalize CX Measurement & Prioritization.pdf";
                var ms = new MemoryStream();
                var writer = new StreamWriter(ms);
                writer.Write(content);
                writer.Flush();
                ms.Position = 0;
                fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
                fileMock.Setup(_ => _.FileName).Returns(fileName);
                fileMock.Setup(_ => _.Length).Returns(ms.Length);
                var file = fileMock.Object;

                var industry = new Industry
                {
                    IndustryName = "Wealth Management"
                };
                var tag = new Tag
                {
                    TagName = "Security & Risk"
                };
                var reportTag = new ReportTags
                {
                    Tag = tag
                };

                var user = new User
                {
                    FirstName = "Jean",
                    LastName = "Birch",
                    Email = "Jean@Birch.com",
                    PhoneNumber = "+99999999999",
                    PasswordHash = "123Jean@"
                };

                var report = new Report
                {
                    ReportName = "Revitalize CX Measurement & Prioritization",
                    Description = "A guide to prioritizing CX projects with the greatest business impact and demonstrating their contributions in a cost-sensitive environment.",
                    Author = user,
                    Industry = industry,
                    FileTitle = "Revitalize CX Measurement & Prioritization.pdf",
                    IsFeatured = true
                };
                user.Reports.Add(report);
                using (var arrangeContext = new InsightHubContext(options))
                {
                    await arrangeContext.Tags.AddAsync(tag);
                    await arrangeContext.Users.AddAsync(user);
                    await arrangeContext.Industries.AddAsync(industry);
                    reportTag.TagID = tag.TagID;
                    await arrangeContext.ReportTags.AddAsync(reportTag);
                    report.ReportTags.Add(reportTag);
                    await arrangeContext.Reports.AddAsync(report);
                    await arrangeContext.SaveChangesAsync();
                }

                //Act & Assert
                using (var assertContext = new InsightHubContext(options))
                {
                    var sut = new ReportService(mockDateTimeProvider.Object, assertContext, mockBlobServices.Object, mockTagServices.Object);
                    var result = await sut.GetCurrentlyFeaturedReportsAsync();

                    Assert.AreEqual(report.ReportName, result.ElementAt(0).ReportName);
                    Assert.AreEqual(report.Description, result.ElementAt(0).Description);
                    Assert.AreEqual(report.Author.Id, result.ElementAt(0).Author.UserID);
                    Assert.AreEqual(report.Industry.IndustryID, result.ElementAt(0).Industry.IndustryID);
                    Assert.AreEqual(report.ReportTags.ElementAt(0).TagID, result.ElementAt(0).ReportTags.ElementAt(0).TagID);
                    Assert.AreEqual(report.FileTitle, result.ElementAt(0).FileTitle);
                }
            }
        }
    }
}
