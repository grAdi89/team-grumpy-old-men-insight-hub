﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.TagServicesTests
{
    [TestClass]
    public class UpdateTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_WhenEditTag_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTag_WhenEditTag_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag tag = new Tag
            {
                TagName = "Security & Risk"
            };

            var editedTag = new TagDTO
            {
                TagID = 1,
                TagName = "Innovation & Strategy"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.UpdateTagAsync(editedTag);
                var expected = await assertContext.Tags.FirstOrDefaultAsync(n => n.TagID == result.TagID);

                Assert.AreEqual(expected.TagName, result.TagName);
                Assert.AreEqual(expected.TagID, result.TagID);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenEdit_TagDoesNotExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenEdit_TagDoesNotExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var editedTag = new TagDTO
            {
                TagID = 1,
                TagName = "Innovation & Strategy"
            };

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateTagAsync(editedTag));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenEdit_Tag_AlreadyExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenEdit_Tag_AlreadyExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();


            Tag firstTag = new Tag
            {
                TagName = "Security & Risk"
            };
            Tag secondTag = new Tag
            {
                TagName = "Innovation & Strategy"
            };

            var editedTag = new TagDTO
            {
                TagID = 1,
                TagName = "Innovation & Strategy"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateTagAsync(editedTag));
            }
        }
    }
}
