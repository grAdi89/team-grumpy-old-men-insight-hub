﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.TagServicesTests
{
    [TestClass]
    public class GetTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_WhenGetTag_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTag_WhenGetTag_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag tag = new Tag
            {
                TagID = 1,
                TagName = "Security & Risk"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetTagAsync(tag.TagID);
                var expected = await assertContext.Tags.FirstOrDefaultAsync(n => n.TagID == result.TagID);

                Assert.AreEqual(expected.TagName, result.TagName);
                Assert.AreEqual(expected.TagID, result.TagID);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenGet_TagDoesNotExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenGet_TagDoesNotExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetTagAsync(1));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenGet_Tag_IsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenGet_Tag_IsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag tag = new Tag
            {
                TagID = 1,
                TagName = "Security & Risk"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                await sut.DeleteTagAsync(tag.TagID);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetTagAsync(1));
            }
        }
    }
}
