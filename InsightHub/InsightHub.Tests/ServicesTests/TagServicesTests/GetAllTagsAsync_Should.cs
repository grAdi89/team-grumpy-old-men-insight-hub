﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.TagServicesTests
{
    [TestClass]
    public class GetAllTagsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTags_WhenGetAllTags_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTags_WhenGetAllTags_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag firstTag = new Tag
            {
                TagID = 1,
                TagName = "Security & Risk"
            };
            Tag secondTag = new Tag
            {
                TagID = 2,
                TagName = "Innovation & Strategy"
            };
            Tag thirdTag = new Tag
            {
                TagID = 3,
                TagName = "Customer Insights & Analytics"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag, thirdTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetAllTagsAsync();
                var expected = await assertContext.Tags.ToListAsync();

                Assert.AreEqual(expected[0].TagName, result.ElementAt(0).TagName);
                Assert.AreEqual(expected[1].TagName, result.ElementAt(1).TagName);
                Assert.AreEqual(expected[2].TagName, result.ElementAt(2).TagName);
            }
        }

        [TestMethod]
        public async Task DoNotReturnTag_WhenTag_IsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DoNotReturnTag_WhenTag_IsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag firstTag = new Tag
            {
                TagID = 1,
                TagName = "Security & Risk"
            };
            Tag secondTag = new Tag
            {
                TagID = 2,
                TagName = "Innovation & Strategy"
            };
            Tag thirdTag = new Tag
            {
                TagID = 3,
                TagName = "Customer Insights & Analytics"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(firstTag, secondTag, thirdTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                await sut.DeleteTagAsync(thirdTag.TagID);
                var result = await sut.GetAllTagsAsync();
                var expected = await assertContext.Tags.ToListAsync();
                int expectedCount = 2;

                Assert.AreEqual(expectedCount, result.Count());
                Assert.AreEqual(expected[0].TagName, result.ElementAt(0).TagName);
                Assert.AreEqual(expected[1].TagName, result.ElementAt(1).TagName);
            }
        }
    }
}
