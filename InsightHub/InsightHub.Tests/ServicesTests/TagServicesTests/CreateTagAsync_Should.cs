﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.TagServicesTests
{
    [TestClass]
    public class CreateTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_WhenCreateTag_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTag_WhenCreateTag_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            string tagName = "Security & Risk";

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.CreateTagAsync(tagName);
                var expected = await assertContext.Tags.FirstOrDefaultAsync(n => n.TagName == result.TagName);

                Assert.AreEqual(tagName, result.TagName);
                Assert.AreEqual(expected.TagName, result.TagName);
                Assert.AreEqual(expected.TagID, result.TagID);
            }
        }
        [TestMethod]
        public async Task ThrowException_WhenCreate_TagExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenCreate_TagExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            string tagName = "Security & Risk";

            var tag = new Tag
            {
                TagName = "Security & Risk"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreateTagAsync(tagName));
            }
        }
    }
}
