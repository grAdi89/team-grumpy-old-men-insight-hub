﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.TagServicesTests
{
    [TestClass]
    public class DeleteTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_WhenDelete_Tag_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_WhenDelete_Tag_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Tag tag = new Tag
            {
                TagName = "Security & Risk"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);
                var createdTag = await assertContext.Tags.FirstOrDefaultAsync(x => x.TagName == tag.TagName);

                var result = await sut.DeleteTagAsync(createdTag.TagID);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenDelete_TagDoesNotExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenDelete_TagDoesNotExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteTagAsync(1));
            }
        }
    }
}
