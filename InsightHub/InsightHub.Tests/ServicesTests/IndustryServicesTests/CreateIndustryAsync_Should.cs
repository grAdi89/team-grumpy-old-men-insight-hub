﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.IndustryServicesTests
{
    [TestClass]
    public class CreateIndustryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIndustry_WhenCreateIndustry_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectIndustry_WhenCreateIndustry_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            string industryName = "Telecommunication industry";

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.CreateIndustryAsync(industryName);
                var expected = await assertContext.Industries.FirstOrDefaultAsync(n => n.IndustryName == result.IndustryName);

                Assert.AreEqual(industryName, result.IndustryName);
                Assert.AreEqual(expected.IndustryName, result.IndustryName);
                Assert.AreEqual(expected.IndustryID, result.IndustryID);
            }
        }

        [TestMethod]
        public async Task CreateIndusty_ShouldThrowException_WhenIndustryAlreadyExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CreateIndusty_ShouldThrowException_WhenIndustryAlreadyExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry industry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };

            string newIndustryName = "Telecommunication industry";

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreateIndustryAsync(newIndustryName));
            }
        }
    }
}
