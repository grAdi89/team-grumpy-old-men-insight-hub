﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.IndustryServicesTests
{
    [TestClass]
    public class DeleteIndustryAsync_Should
    {
        [TestMethod]
        public async Task DeleteIndustry_Successfully()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeleteIndustry_Successfully));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry industry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteIndustryAsync(1);
                var expected = await assertContext.Industries.FirstOrDefaultAsync(n => n.IndustryID == industry.IndustryID && !n.IsDeleted);

                Assert.AreEqual(true, result);
                Assert.AreEqual(null, expected);
            }
        }

        [TestMethod]
        public async Task DeleteIndusty_ThrowException_WhenIndustryNotFound()
        {
            var options = Utils.GetOptions(nameof(DeleteIndusty_ThrowException_WhenIndustryNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            
            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteIndustryAsync(1));
            }
        }
    }
}
