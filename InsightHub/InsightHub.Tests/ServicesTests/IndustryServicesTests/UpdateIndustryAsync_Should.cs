﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.IndustryServicesTests
{
    [TestClass]
    public class UpdateIndustryAsync_Should
    {
        [TestMethod]
        public async Task UpdateIndustry_WhenIndustryExist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdateIndustry_WhenIndustryExist));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry industry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IndustryCreateDTO industryDTO = new IndustryCreateDTO { IndustryID = industry.IndustryID, IndustryName = "Food Industry" };

                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.UpdateIndustryAsync(industryDTO);
                var expected = await assertContext.Industries.FirstOrDefaultAsync(n => n.IndustryID == result.IndustryID);

                Assert.AreEqual(expected.IndustryName, result.IndustryName);
                Assert.AreEqual(expected.IndustryID, result.IndustryID);
            }
        }

        [TestMethod]
        public async Task UpdateIndustry_ThrowException_WhenIndustryNotFound()
        {
            var options = Utils.GetOptions(nameof(UpdateIndustry_ThrowException_WhenIndustryNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var assertContext = new InsightHubContext(options))
            {
                IndustryCreateDTO industryDTO = new IndustryCreateDTO { IndustryID = 1, IndustryName = "Food Industry" };

                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateIndustryAsync(industryDTO));
            }
        }
    }
}
