﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.IndustryServicesTests
{
    [TestClass]
    public class GetAllIndustriesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIndustries_WhenGetAllIndustries_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectIndustries_WhenGetAllIndustries_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry firstIndustry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };
            Industry secondIndustry = new Industry
            {
                IndustryID = 2,
                IndustryName = "Computer Industry"
            };
            Industry thirdIndustry = new Industry
            {
                IndustryID = 3,
                IndustryName = "Transport Industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(firstIndustry, secondIndustry, thirdIndustry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetAllIndustriesAsync();
                var expected = await assertContext.Industries.ToListAsync();

                Assert.AreEqual(expected[0].IndustryName, result.ElementAt(0).IndustryName);
                Assert.AreEqual(expected[1].IndustryName, result.ElementAt(1).IndustryName);
                Assert.AreEqual(expected[2].IndustryName, result.ElementAt(2).IndustryName);
            }
        }

        [TestMethod]
        public async Task DoNotReturnIndustry_WhenIndustry_IsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DoNotReturnIndustry_WhenIndustry_IsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry firstIndustry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };
            Industry secondIndustry = new Industry
            {
                IndustryID = 2,
                IndustryName = "Computer Industry"
            };
            Industry thirdIndustry = new Industry
            {
                IndustryID = 3,
                IndustryName = "Transport Industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(firstIndustry, secondIndustry, thirdIndustry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                await sut.DeleteIndustryAsync(thirdIndustry.IndustryID);
                var result = await sut.GetAllIndustriesAsync();
                var expected = await assertContext.Industries.ToListAsync();
                int expectedCount = 2;

                Assert.AreEqual(expectedCount, result.Count());
                Assert.AreEqual(expected[0].IndustryName, result.ElementAt(0).IndustryName);
                Assert.AreEqual(expected[1].IndustryName, result.ElementAt(1).IndustryName);
            }
        }
    }
}
