﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ServicesTests.IndustryServicesTests
{
    [TestClass]
    public class GetIndustryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIndustry_WhenGetIndustry_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectIndustry_WhenGetIndustry_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry industry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetIndustryAsync(industry.IndustryID);
                var expected = await assertContext.Industries.FirstOrDefaultAsync(n => n.IndustryName == result.IndustryName);

                Assert.AreEqual(expected.IndustryName, result.IndustryName);
                Assert.AreEqual(expected.IndustryID, result.IndustryID);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenGet_IndustryDoesNotExists()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenGet_IndustryDoesNotExists));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetIndustryAsync(1));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenGet_Industry_IsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenGet_Industry_IsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            Industry industry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Telecommunication industry"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryServices(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteIndustryAsync(industry.IndustryID);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetIndustryAsync(industry.IndustryID));
            }
        }
    }
}
