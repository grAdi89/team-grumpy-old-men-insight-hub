﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Subscription entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    public class SubscriptionConfig : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.HasKey(db => new
            {
                db.UserID,
                db.IndustryID
            });

            builder.HasOne(i => i.Industry)
                .WithMany(sb => sb.SubscribedBy)
                .HasForeignKey(k => k.IndustryID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();


            builder.HasOne(u => u.User)
                .WithMany(st => st.SubscribedTo)
                .HasForeignKey(k => k.UserID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
