﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Industry entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    public class IndustryConfig : IEntityTypeConfiguration<Industry>
    {
        public void Configure(EntityTypeBuilder<Industry> builder)
        {
            builder.HasKey(report => report.IndustryID);

            builder.Property(report => report.IndustryName)
                .IsRequired();

            builder.HasMany(r => r.Reports)
                .WithOne(i => i.Industry)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(s => s.SubscribedBy)
                .WithOne(i => i.Industry)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
