﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of ReportTags entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    public class ReportTagsConfig : IEntityTypeConfiguration<ReportTags>
    {
        public void Configure(EntityTypeBuilder<ReportTags> builder)
        {
            builder.HasKey(db => new
            {
                db.TagID,
                db.ReportID
            });

            builder.HasOne(r => r.Report)
                .WithMany(rt => rt.ReportTags)
                .HasForeignKey(k => k.ReportID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();


            builder.HasOne(t => t.Tag)
                .WithMany(tr => tr.TagReports)
                .HasForeignKey(k => k.TagID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
