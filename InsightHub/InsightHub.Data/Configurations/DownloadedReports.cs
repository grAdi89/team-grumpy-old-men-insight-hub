﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of DowloadedReports entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    public class DowloadedReportsConfig : IEntityTypeConfiguration<DownloadedReports>
    {
        public void Configure(EntityTypeBuilder<DownloadedReports> builder)
        {
            builder.HasKey(db => new
            {
                db.UserID,
                db.ReportID
            });

            builder.HasOne(r => r.Report)
                .WithMany(dr => dr.DownloadedReport)
                .HasForeignKey(k => k.ReportID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();


            builder.HasOne(u => u.User)
                .WithMany(rd => rd.ReportsDowloaded)
                .HasForeignKey(k => k.UserID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
