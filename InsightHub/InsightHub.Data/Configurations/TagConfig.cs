﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Tag entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    public class TagConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasKey(tag => tag.TagID);

            builder.Property(tag => tag.TagName)
                .IsRequired();

            builder.HasMany(r => r.TagReports)
                .WithOne(t => t.Tag)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
