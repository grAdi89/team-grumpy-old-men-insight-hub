﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Report entity.
    /// It inherits Microsoft.EntityFrameworkCore.IEntityTypeConfiguration.
    /// </summary>
    class ReportConfig : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> builder)
        {
            builder.HasKey(report => report.ReportID);

            builder.Property(report => report.ReportName)
                .IsRequired();

            builder.Property(descr => descr.Description)
                .IsRequired();

            builder.HasOne(author => author.Author)
                .WithMany(r => r.Reports)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(industry => industry.Industry)
                .WithMany(r => r.Reports)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(rt => rt.ReportTags)
                .WithOne(r => r.Report)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(dr => dr.DownloadedReport)
                .WithOne(r => r.Report)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
