﻿using InsightHub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace InsightHub.Data.Seeder
{
    /// <summary>
    /// A class that seeds entities for the models in the database.
    /// </summary>
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {
            builder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new Role
                {
                    Id = 2,
                    Name = "Author",
                    NormalizedName = "AUTHOR"
                },
                new Role
                {
                    Id = 3,
                    Name = "Customer",
                    NormalizedName = "CUSTOMER"
                }
                );

            var hasher = new PasswordHasher<User>();

            var admin = new User
            {
                Id = 1,
                FirstName = "James",
                LastName = "Bond",
                Email = "admin@admin.com",
                NormalizedEmail = "ADMIN@ADMIN.COM",
                UserName = "admin@admin.com",
                NormalizedUserName = "ADMIN@ADMIN.COM",
                IsApprovedByAdmin = true,
                PhoneNumber = "+666 6666 666 666",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            admin.PasswordHash = hasher.HashPassword(admin, "@Admin123");

            builder.Entity<User>().HasData(admin);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = admin.Id
                });

            var firstAuthor = new User
            {
                Id = 2,
                FirstName = "Bobby",
                LastName = "Cameron",
                Email = "telerik_author@abv.bg",
                NormalizedEmail = "TELERIK_AUTHOR@ABV.BG",
                UserName = "telerik_author@abv.bg",
                NormalizedUserName = "TELERIK_AUTHOR@ABV.BG",
                IsApprovedByAdmin = true,
                PhoneNumber = "+359 888 908 701",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            firstAuthor.PasswordHash = hasher.HashPassword(firstAuthor, "@telerik_author123");

            builder.Entity<User>().HasData(firstAuthor);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = firstAuthor.Id
                });

            var secondAuthor = new User
            {
                Id = 3,
                FirstName = "Andrew",
                LastName = "Bartels",
                Email = "andrew@bartels.com",
                NormalizedEmail = "ANDREW@BARTELS.COM",
                UserName = "andrew@bartels.com",
                NormalizedUserName = "ANDREW@BARTELS.COM",
                IsApprovedByAdmin = true,
                PhoneNumber = "+359 886 108 221",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            secondAuthor.PasswordHash = hasher.HashPassword(secondAuthor, "@andrew123");

            builder.Entity<User>().HasData(secondAuthor);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = secondAuthor.Id
                });

            var customer = new User
            {
                Id = 4,
                FirstName = "Steven",
                LastName = "Donaldson",
                Email = "steven@donaldson.com",
                NormalizedEmail = "STEVEN@DONALDSON.COM",
                UserName = "steven@donaldson.com",
                NormalizedUserName = "STEVEN@DONALDSON.COM",
                IsApprovedByAdmin = true,
                PhoneNumber = "+359 886 909 322",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            customer.PasswordHash = hasher.HashPassword(customer, "@steven123");

            builder.Entity<User>().HasData(customer);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 3,
                    UserId = customer.Id
                });

            //Tags

            var firstTag = new Tag
            {
                TagID = 1,
                TagName = "Business & IT Alignment",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(firstTag);

            var secondTag = new Tag
            {
                TagID = 2,
                TagName = "Enterprise Architecture Domains & Practices",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(secondTag);

            var thirdTag = new Tag
            {
                TagID = 3,
                TagName = "Business Technology (BT)",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(thirdTag);

            var fourthTag = new Tag
            {
                TagID = 4,
                TagName = "Capability Road Map",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(fourthTag);

            var fifthTag = new Tag
            {
                TagID = 5,
                TagName = "Enterprise Content Management (ECM)",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(fifthTag);

            var sixthTag = new Tag
            {
                TagID = 6,
                TagName = "Information Architecture",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(sixthTag);

            var seventhTag = new Tag
            {
                TagID = 7,
                TagName = "Business Value",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(seventhTag);

            var eighthTag = new Tag
            {
                TagID = 8,
                TagName = "Cloud Computing",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(eighthTag);

            var ninthTag = new Tag
            {
                TagID = 9,
                TagName = "Cloud Security",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(ninthTag);

            var tenthTag = new Tag
            {
                TagID = 10,
                TagName = "Cost Control",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(tenthTag);

            var eleventhTag = new Tag
            {
                TagID = 11,
                TagName = "Customer Experience Management",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(eleventhTag);

            var twelvethTag = new Tag
            {
                TagID = 12,
                TagName = "Customer Experience Measurement",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(twelvethTag);

            var thirteenthTag = new Tag
            {
                TagID = 13,
                TagName = "Customer Experience Strategy",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(thirteenthTag);

            var fourteenthTag = new Tag
            {
                TagID = 14,
                TagName = "Employee Experience",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(fourteenthTag);

            var fifteenthTag = new Tag
            {
                TagID = 15,
                TagName = "Architecture & Technology Strategy",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(fifteenthTag);

            var sixteenthTag = new Tag
            {
                TagID = 16,
                TagName = "Banking",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(sixteenthTag);

            var seventeenthTag = new Tag
            {
                TagID = 17,
                TagName = "Business Models",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(seventeenthTag);

            var eighteenthTag = new Tag
            {
                TagID = 18,
                TagName = "Digital Banking",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(eighteenthTag);

            var nineteenthTag = new Tag
            {
                TagID = 19,
                TagName = "Digital Business",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(nineteenthTag);

            var twentiethTag = new Tag
            {
                TagID = 20,
                TagName = "Financial Services",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Tag>().HasData(twentiethTag);

            //Industries

            var firstIndustry = new Industry
            {
                IndustryID = 1,
                IndustryName = "Financial Services",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(firstIndustry);

            var secondIndustry = new Industry
            {
                IndustryID = 2,
                IndustryName = "Retail",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(secondIndustry);

            var thirdIndustry = new Industry
            {
                IndustryID = 3,
                IndustryName = "Healthcare",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(thirdIndustry);

            var fourthIndustry = new Industry
            {
                IndustryID = 4,
                IndustryName = "Manufacturing",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(fourthIndustry);

            var fifthIndustry = new Industry
            {
                IndustryID = 5,
                IndustryName = "Transportation & Logistics",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(fifthIndustry);

            var sixthIndustry = new Industry
            {
                IndustryID = 6,
                IndustryName = "Public Sector",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(sixthIndustry);

            var seventhIndustry = new Industry
            {
                IndustryID = 7,
                IndustryName = "Consumer Electronics",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(seventhIndustry);

            var eighthIndustry = new Industry
            {
                IndustryID = 8,
                IndustryName = "Energy & Utilities",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(eighthIndustry);

            var ninthIndustry = new Industry
            {
                IndustryID = 9,
                IndustryName = "Media & Entertainment",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(ninthIndustry);

            var tenthIndustry = new Industry
            {
                IndustryID = 10,
                IndustryName = "Travel",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false
            };

            builder.Entity<Industry>().HasData(tenthIndustry);

            //Reports

            var firstReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF00"),
                ReportName = "Smaller Banks Are Turning To Fintechs",
                Description = "Results From Global Banking Platform Deals Survey 2020.",
                AuthorID = firstAuthor.Id,
                IndustryID = firstIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "The Banking Platform Market.pdf"
            };

            ReportTags firstReportTag = new ReportTags
            { ReportID = firstReport.ReportID, TagID = sixteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(firstReportTag);
            ReportTags secondReportTag = new ReportTags
            { ReportID = firstReport.ReportID, TagID = twentiethTag.TagID };
            builder.Entity<ReportTags>().HasData(secondReportTag);
            builder.Entity<Report>().HasData(firstReport);

            var secondReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF01"),
                ReportName = "Open Banking Intermediaries, Q2",
                Description = "Landscape Overview Of 35 Providers.",
                AuthorID = secondAuthor.Id,
                IndustryID = firstIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Landscape Overview Of 35 Providers.pdf"
            };

            ReportTags thirdReportTag = new ReportTags
            { ReportID = secondReport.ReportID, TagID = fifteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirdReportTag);
            ReportTags fourthReportTag = new ReportTags
            { ReportID = secondReport.ReportID, TagID = thirdTag.TagID };
            builder.Entity<ReportTags>().HasData(fourthReportTag);
            ReportTags fifthReportTag = new ReportTags
            { ReportID = secondReport.ReportID, TagID = sixteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(fifthReportTag);
            builder.Entity<Report>().HasData(secondReport);

            var thirdReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF02"),
                ReportName = "Insurtech Funding Roundup",
                Description = "Insurtechs have brought innovation across the insurance value chain.",
                AuthorID = secondAuthor.Id,
                IndustryID = firstIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Insurtech Funding.pdf"
            };

            ReportTags sixthReportTag = new ReportTags
            { ReportID = thirdReport.ReportID, TagID = firstTag.TagID };
            builder.Entity<ReportTags>().HasData(sixthReportTag);
            ReportTags seventhReportTag = new ReportTags
            { ReportID = thirdReport.ReportID, TagID = secondTag.TagID };
            builder.Entity<ReportTags>().HasData(seventhReportTag);
            ReportTags eighthReportTag = new ReportTags
            { ReportID = thirdReport.ReportID, TagID = eighthTag.TagID };
            builder.Entity<ReportTags>().HasData(eighthReportTag);
            builder.Entity<Report>().HasData(thirdReport);

            var fourthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF03"),
                ReportName = "Guide: How To Use The Moments Index",
                Description = "Moments Index To Make Smart DX Channel Choices.",
                AuthorID = firstAuthor.Id,
                IndustryID = sixthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "How To Use The Moments Index.pdf"
            };

            ReportTags ninthReportTag = new ReportTags
            { ReportID = fourthReport.ReportID, TagID = eleventhTag.TagID };
            builder.Entity<ReportTags>().HasData(ninthReportTag);
            ReportTags tenthReportTag = new ReportTags
            { ReportID = fourthReport.ReportID, TagID = twentiethTag.TagID };
            builder.Entity<ReportTags>().HasData(tenthReportTag);
            ReportTags eleventhReportTag = new ReportTags
            { ReportID = fourthReport.ReportID, TagID = seventeenthTag.TagID };
            builder.Entity<ReportTags>().HasData(eleventhReportTag);
            builder.Entity<Report>().HasData(fourthReport);

            var fifthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF04"),
                ReportName = "Internet Of Things",
                Description = "Eighteen Technologies Underpin IoT Solutions",
                AuthorID = firstAuthor.Id,
                IndustryID = fifthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Tech Tide.pdf"
            };

            ReportTags twelvethReportTag = new ReportTags
            { ReportID = fifthReport.ReportID, TagID = nineteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(twelvethReportTag);
            ReportTags thirteenthReportTag = new ReportTags
            { ReportID = fifthReport.ReportID, TagID = seventeenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirteenthReportTag);
            builder.Entity<Report>().HasData(fifthReport);

            var sixthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF05"),
                ReportName = "Smart Speakers Lead The Smart Home Revolution",
                Description = "Cost Continues To Be The Biggest Inhibitor To Smart Home Adoption",
                AuthorID = firstAuthor.Id,
                IndustryID = secondIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Customer Insights Professionals.pdf"
            };

            ReportTags fourteenthReportTag = new ReportTags
            { ReportID = sixthReport.ReportID, TagID = thirdTag.TagID };
            builder.Entity<ReportTags>().HasData(fourteenthReportTag);
            ReportTags fifteenthReportTag = new ReportTags
            { ReportID = sixthReport.ReportID, TagID = fourteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(fifteenthReportTag);
            builder.Entity<Report>().HasData(sixthReport);

            var seventhReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF06"),
                ReportName = "Digital Voice Experiences",
                Description = "A Digital Voice Experience Series Report",
                AuthorID = firstAuthor.Id,
                IndustryID = fourthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Digital Voice Experiences.pdf"
            };

            ReportTags sixteenthReportTag = new ReportTags
            { ReportID = seventhReport.ReportID, TagID = fifthTag.TagID };
            builder.Entity<ReportTags>().HasData(sixteenthReportTag);
            ReportTags seventeenthReportTag = new ReportTags
            { ReportID = seventhReport.ReportID, TagID = ninthTag.TagID };
            builder.Entity<ReportTags>().HasData(seventeenthReportTag);
            builder.Entity<Report>().HasData(seventhReport);

            var eighthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF07"),
                ReportName = "The CMO's Guide To CES 2019: Five Big Brand Ideas",
                Description = "CES, the world's largest tech show, is about much more than technology.",
                AuthorID = firstAuthor.Id,
                IndustryID = thirdIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Five Big Brand Ideas.pdf"
            };

            ReportTags eighteenthReportTag = new ReportTags
            { ReportID = eighthReport.ReportID, TagID = sixthTag.TagID };
            builder.Entity<ReportTags>().HasData(eighteenthReportTag);
            ReportTags nineteenthReportTag = new ReportTags
            { ReportID = eighthReport.ReportID, TagID = seventhTag.TagID };
            builder.Entity<ReportTags>().HasData(nineteenthReportTag);
            builder.Entity<Report>().HasData(eighthReport);

            var ninthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF08"),
                ReportName = "The Top Retail Technology Investments In 2019",
                Description = "Retailers Must Distinguish Hot From Hype In Tech Investments.",
                AuthorID = firstAuthor.Id,
                IndustryID = seventhIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Retail Technology Investments.pdf"
            };

            ReportTags twentiethReportTag = new ReportTags
            { ReportID = ninthReport.ReportID, TagID = eighthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentiethReportTag);
            ReportTags twentyfirsthReportTag = new ReportTags
            { ReportID = ninthReport.ReportID, TagID = ninthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyfirsthReportTag);
            builder.Entity<Report>().HasData(ninthReport);

            var tenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF09"),
                ReportName = "The 3D Connected Consumer In 2019",
                Description = "Create A Portfolio Of Experiences That Maps To How Your Customers Connect",
                AuthorID = secondAuthor.Id,
                IndustryID = eighthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "3D Connected Consumer.pdf"
            };

            ReportTags twentysecondReportTag = new ReportTags
            { ReportID = tenthReport.ReportID, TagID = eighthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentysecondReportTag);
            ReportTags twentythurdReportTag = new ReportTags
            { ReportID = tenthReport.ReportID, TagID = ninthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentythurdReportTag);
            ReportTags twentyfourthReportTag = new ReportTags
            { ReportID = tenthReport.ReportID, TagID = tenthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyfourthReportTag);
            builder.Entity<Report>().HasData(tenthReport);

            var eleventhReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF10"),
                ReportName = "The Future Of Privacy: Personal Identity And Data Management",
                Description = "Consumer-Managed Data Is The Future Of The Data Economy",
                AuthorID = secondAuthor.Id,
                IndustryID = ninthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Personal Identity And Data Management.pdf"
            };

            ReportTags twentyfifthReportTag = new ReportTags
            { ReportID = eleventhReport.ReportID, TagID = eleventhTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyfifthReportTag);
            ReportTags twentysixthReportTag = new ReportTags
            { ReportID = eleventhReport.ReportID, TagID = twelvethTag.TagID };
            builder.Entity<ReportTags>().HasData(twentysixthReportTag);
            builder.Entity<Report>().HasData(eleventhReport);

            var twelvethReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF11"),
                ReportName = "The European Consumer Tech Stack",
                Description = "Our 2018 European Benchmark Data Overview Shows The Four Forces Of Adoption",
                AuthorID = secondAuthor.Id,
                IndustryID = tenthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "European Consumer Tech Stack.pdf"
            };

            ReportTags twentyseventhReportTag = new ReportTags
            { ReportID = twelvethReport.ReportID, TagID = thirteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyseventhReportTag);
            ReportTags twentyeightReportTag = new ReportTags
            { ReportID = twelvethReport.ReportID, TagID = fourteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyeightReportTag);
            ReportTags twentyninthReportTag = new ReportTags
            { ReportID = twelvethReport.ReportID, TagID = fifteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(twentyninthReportTag);
            builder.Entity<Report>().HasData(twelvethReport);

            var thirteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF12"),
                ReportName = "Experiment With New Approaches",
                Description = "Advanced Level: Design Practices For CX Transformation",
                AuthorID = secondAuthor.Id,
                IndustryID = tenthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Leading Edge Of Design.pdf"
            };

            ReportTags thirtiethReportTag = new ReportTags
            { ReportID = thirteenthReport.ReportID, TagID = sixteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtiethReportTag);
            ReportTags thirtyfirstReportTag = new ReportTags
            { ReportID = thirteenthReport.ReportID, TagID = seventeenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyfirstReportTag);
            builder.Entity<Report>().HasData(thirteenthReport);

            var fourteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF13"),
                ReportName = "US Consumers And Technology, 2019",
                Description = "Increasingly Intimate Technologies Spark The Next Mind Shift",
                AuthorID = firstAuthor.Id,
                IndustryID = firstIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "US Consumers And Technology 2019.pdf"
            };

            ReportTags thirtysecondReportTag = new ReportTags
            { ReportID = fourteenthReport.ReportID, TagID = sixteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtysecondReportTag);
            ReportTags thirtythirdReportTag = new ReportTags
            { ReportID = fourteenthReport.ReportID, TagID = seventeenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtythirdReportTag);
            builder.Entity<Report>().HasData(fourteenthReport);

            var fifteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF14"),
                ReportName = "Smart Home Revolution",
                Description = "Cost Continues To Be The Biggest Inhibitor To Smart Home Adoption",
                AuthorID = secondAuthor.Id,
                IndustryID = secondIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Smart Home Revolution.pdf"
            };

            ReportTags thirtyfourthReportTag = new ReportTags
            { ReportID = fifteenthReport.ReportID, TagID = eighteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyfourthReportTag);
            ReportTags thirtyfifthReportTag = new ReportTags
            { ReportID = fifteenthReport.ReportID, TagID = nineteenthTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyfifthReportTag);
            builder.Entity<Report>().HasData(fifteenthReport);

            var sixteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF15"),
                ReportName = "The UK Retail Customer Experience Index, 2019",
                Description = "How UK Retail Brands Earn Loyalty With The Quality Of Their Experience",
                AuthorID = secondAuthor.Id,
                IndustryID = thirdIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "UK Retail Customer Experience.pdf"
            };

            ReportTags thirtysixthReportTag = new ReportTags
            { ReportID = sixteenthReport.ReportID, TagID = twentiethTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtysixthReportTag);
            ReportTags thirtyseventhReportTag = new ReportTags
            { ReportID = sixteenthReport.ReportID, TagID = firstTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyseventhReportTag);
            builder.Entity<Report>().HasData(sixteenthReport);

            var seventeenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF16"),
                ReportName = "The Future Of Work In Banking",
                Description = "Routine Compliance And Branch Management.",
                AuthorID = firstAuthor.Id,
                IndustryID = fourthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "The Future Of Work In Banking.pdf"
            };

            ReportTags thirtyeighthReportTag = new ReportTags
            { ReportID = seventeenthReport.ReportID, TagID = secondTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyeighthReportTag);
            ReportTags thirtyninthReportTag = new ReportTags
            { ReportID = seventeenthReport.ReportID, TagID = thirdTag.TagID };
            builder.Entity<ReportTags>().HasData(thirtyninthReportTag);
            builder.Entity<Report>().HasData(seventeenthReport);

            var eighteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF17"),
                ReportName = "Mobile Commerce In 2030",
                Description = "mCommerce Is A New Mobile Services Economy, Not Mini eCommerce",
                AuthorID = firstAuthor.Id,
                IndustryID = fifthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Mobile Commerce In 2030.pdf"
            };

            ReportTags fortiethReportTag = new ReportTags
            { ReportID = eighteenthReport.ReportID, TagID = fourthTag.TagID };
            builder.Entity<ReportTags>().HasData(fortiethReportTag);
            ReportTags fourtyfirsthReportTag = new ReportTags
            { ReportID = eighteenthReport.ReportID, TagID = fifthTag.TagID };
            builder.Entity<ReportTags>().HasData(fourtyfirsthReportTag);
            builder.Entity<Report>().HasData(eighteenthReport);

            var nineteenthReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF18"),
                ReportName = "Technology Investments In Insurance In 2020",
                Description = "Tools And Technology: The Digital Insurance Strategy Playbook",
                AuthorID = secondAuthor.Id,
                IndustryID = sixthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Insurance In 2020.pdf"
            };

            ReportTags fourtysecondReportTag = new ReportTags
            { ReportID = nineteenthReport.ReportID, TagID = sixthTag.TagID };
            builder.Entity<ReportTags>().HasData(fourtysecondReportTag);
            ReportTags fourtythirdReportTag = new ReportTags
            { ReportID = nineteenthReport.ReportID, TagID = seventhTag.TagID };
            builder.Entity<ReportTags>().HasData(fourtythirdReportTag);
            builder.Entity<Report>().HasData(nineteenthReport);

            var twentiethReport = new Report
            {
                ReportID = new Guid("11223344-5566-7788-99AA-BBCCDDEEFF19"),
                ReportName = "The Future Of Small Business Banking",
                Description = "Bank Brands Will Fade As Their SME Products Become Components In Other Companies' Ecosystems",
                AuthorID = secondAuthor.Id,
                IndustryID = sixthIndustry.IndustryID,
                CreatedOn = DateTime.UtcNow,
                FileTitle = "Small Business Banking.pdf"
            };

            ReportTags fourtyfourthReportTag = new ReportTags
            { ReportID = twentiethReport.ReportID, TagID = eighthTag.TagID };
            builder.Entity<ReportTags>().HasData(fourtyfourthReportTag);
            ReportTags fourtyfifthReportTag = new ReportTags
            { ReportID = twentiethReport.ReportID, TagID = ninthTag.TagID };
            builder.Entity<ReportTags>().HasData(fourtyfifthReportTag);
            builder.Entity<Report>().HasData(twentiethReport);
        }
    }
}
