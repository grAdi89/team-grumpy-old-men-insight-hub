﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsApprovedByAdmin = table.Column<bool>(nullable: false),
                    IsBanned = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DeleteOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    IndustryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IndustryName = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.IndustryID);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    TagID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagName = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.TagID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    ReportID = table.Column<Guid>(nullable: false),
                    ReportName = table.Column<string>(maxLength: 60, nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: false),
                    AuthorID = table.Column<int>(nullable: false),
                    IndustryID = table.Column<int>(nullable: false),
                    FileTitle = table.Column<string>(maxLength: 65, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    FeaturedOn = table.Column<DateTime>(nullable: false),
                    DownloadCount = table.Column<int>(nullable: false),
                    IsFeatured = table.Column<bool>(nullable: false),
                    IsPending = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.ReportID);
                    table.ForeignKey(
                        name: "FK_Reports_AspNetUsers_AuthorID",
                        column: x => x.AuthorID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reports_Industries_IndustryID",
                        column: x => x.IndustryID,
                        principalTable: "Industries",
                        principalColumn: "IndustryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions",
                columns: table => new
                {
                    IndustryID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => new { x.UserID, x.IndustryID });
                    table.ForeignKey(
                        name: "FK_Subscriptions_Industries_IndustryID",
                        column: x => x.IndustryID,
                        principalTable: "Industries",
                        principalColumn: "IndustryID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subscriptions_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DownloadedReports",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false),
                    ReportID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DownloadedReports", x => new { x.UserID, x.ReportID });
                    table.ForeignKey(
                        name: "FK_DownloadedReports_Reports_ReportID",
                        column: x => x.ReportID,
                        principalTable: "Reports",
                        principalColumn: "ReportID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DownloadedReports_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReportTags",
                columns: table => new
                {
                    TagID = table.Column<int>(nullable: false),
                    ReportID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTags", x => new { x.TagID, x.ReportID });
                    table.ForeignKey(
                        name: "FK_ReportTags_Reports_ReportID",
                        column: x => x.ReportID,
                        principalTable: "Reports",
                        principalColumn: "ReportID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReportTags_Tags_TagID",
                        column: x => x.TagID,
                        principalTable: "Tags",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 3, "e7b3547f-be19-4521-8cda-10af54bef262", "Customer", "CUSTOMER" },
                    { 2, "242746d7-4c76-40ac-be5c-f13d79b57af7", "Author", "AUTHOR" },
                    { 1, "16b57eeb-486c-4667-9556-0aefb5d143e0", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeleteOn", "Email", "EmailConfirmed", "FirstName", "IsApprovedByAdmin", "IsBanned", "IsDeleted", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 4, 0, "1ca37683-01f8-4220-9937-a9258a07370c", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "steven@donaldson.com", false, "Steven", true, false, false, "Donaldson", false, null, "STEVEN@DONALDSON.COM", "STEVEN@DONALDSON.COM", "AQAAAAEAACcQAAAAEJWXQWfoIU6HfAgfTxlXShuBxkTP/rKmHlkVknx3li29m55Pzml2mKCckt1S0F+8hQ==", "+359 886 909 322", false, "4af0328d-78da-4cd8-8313-8b02b89793ee", false, "steven@donaldson.com" },
                    { 2, 0, "e6de545a-4a1c-4729-a9fd-575e01616f86", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "telerik_author@abv.bg", false, "Bobby", true, false, false, "Cameron", false, null, "TELERIK_AUTHOR@ABV.BG", "TELERIK_AUTHOR@ABV.BG", "AQAAAAEAACcQAAAAEM2TZRX140hu3uFVYLCQVH3rOdrY7bvAGviyOV7dam2VkaF/LD2axur/FfXppHJ8nA==", "+359 888 908 701", false, "1297729b-5e8b-4052-914d-f5e58c56223b", false, "telerik_author@abv.bg" },
                    { 1, 0, "f2ab28bf-bd64-401a-afce-41d3196c2e50", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "admin@admin.com", false, "James", true, false, false, "Bond", false, null, "ADMIN@ADMIN.COM", "ADMIN@ADMIN.COM", "AQAAAAEAACcQAAAAEA9VcJqGYvwW/aPvlWpa3v3oNvI3XFu4ARtPW7SG34RPr/WjZmRvzipQYckKndHR3g==", "+666 6666 666 666", false, "3cc9c86f-cc67-46d9-ad14-73a2b78e1da4", false, "admin@admin.com" },
                    { 3, 0, "d1704f50-d124-4480-b080-4b494778c810", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "andrew@bartels.com", false, "Andrew", true, false, false, "Bartels", false, null, "ANDREW@BARTELS.COM", "ANDREW@BARTELS.COM", "AQAAAAEAACcQAAAAEHa6O5kLfBJ2mUuWQ8BXIM997x8d5w1tY6b1Fpp6MgndR/hPWbAlbakPR4YsVNa8zA==", "+359 886 108 221", false, "a829832c-0d97-4b08-b221-a36ecf7f7860", false, "andrew@bartels.com" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "IndustryID", "CreatedOn", "DeletedOn", "IndustryName", "IsDeleted" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(749), null, "Travel", false },
                    { 1, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(9729), null, "Financial Services", false },
                    { 8, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(722), null, "Energy & Utilities", false },
                    { 7, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(707), null, "Consumer Electronics", false },
                    { 6, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(641), null, "Public Sector", false },
                    { 5, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(626), null, "Transportation & Logistics", false },
                    { 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(542), null, "Retail", false },
                    { 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(598), null, "Healthcare", false },
                    { 4, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(613), null, "Manufacturing", false },
                    { 9, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(736), null, "Media & Entertainment", false }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "TagID", "CreatedOn", "DeletedOn", "IsDeleted", "TagName" },
                values: new object[,]
                {
                    { 20, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7816), null, false, "Financial Services" },
                    { 19, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7803), null, false, "Digital Business" },
                    { 18, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7790), null, false, "Digital Banking" },
                    { 17, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7776), null, false, "Business Models" },
                    { 16, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7763), null, false, "Banking" },
                    { 15, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7750), null, false, "Architecture & Technology Strategy" },
                    { 14, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7737), null, false, "Employee Experience" },
                    { 13, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7723), null, false, "Customer Experience Strategy" },
                    { 10, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7683), null, false, "Cost Control" },
                    { 11, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7697), null, false, "Customer Experience Management" },
                    { 9, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7669), null, false, "Cloud Security" },
                    { 8, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7655), null, false, "Cloud Computing" },
                    { 7, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7642), null, false, "Business Value" },
                    { 5, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7575), null, false, "Enterprise Content Management (ECM)" },
                    { 4, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7562), null, false, "Capability Road Map" },
                    { 3, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7547), null, false, "Business Technology (BT)" },
                    { 2, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7501), null, false, "Enterprise Architecture Domains & Practices" },
                    { 1, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(6537), null, false, "Business & IT Alignment" },
                    { 12, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7710), null, false, "Customer Experience Measurement" },
                    { 6, new DateTime(2020, 6, 8, 18, 20, 23, 166, DateTimeKind.Utc).AddTicks(7627), null, false, "Information Architecture" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 3, 2 },
                    { 2, 2 },
                    { 4, 3 }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "ReportID", "AuthorID", "CreatedOn", "Description", "DownloadCount", "FeaturedOn", "FileTitle", "IndustryID", "IsFeatured", "IsPending", "ReportName" },
                values: new object[,]
                {
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff19"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7512), "Bank Brands Will Fade As Their SME Products Become Components In Other Companies' Ecosystems", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Small Business Banking.pdf", 6, false, false, "The Future Of Small Business Banking" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff18"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7470), "Tools And Technology: The Digital Insurance Strategy Playbook", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Insurance In 2020.pdf", 6, false, false, "Technology Investments In Insurance In 2020" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff15"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7320), "How UK Retail Brands Earn Loyalty With The Quality Of Their Experience", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "UK Retail Customer Experience.pdf", 3, false, false, "The UK Retail Customer Experience Index, 2019" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff14"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7277), "Cost Continues To Be The Biggest Inhibitor To Smart Home Adoption", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smart Home Revolution.pdf", 2, false, false, "Smart Home Revolution" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff12"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7193), "Advanced Level: Design Practices For CX Transformation", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Leading Edge Of Design.pdf", 10, false, false, "Experiment With New Approaches" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff11"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7138), "Our 2018 European Benchmark Data Overview Shows The Four Forces Of Adoption", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "European Consumer Tech Stack.pdf", 10, false, false, "The European Consumer Tech Stack" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff10"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7073), "Consumer-Managed Data Is The Future Of The Data Economy", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Personal Identity And Data Management.pdf", 9, false, false, "The Future Of Privacy: Personal Identity And Data Management" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff09"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7021), "Create A Portfolio Of Experiences That Maps To How Your Customers Connect", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "3D Connected Consumer.pdf", 8, false, false, "The 3D Connected Consumer In 2019" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff03"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6725), "Moments Index To Make Smart DX Channel Choices.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "How To Use The Moments Index.pdf", 6, false, false, "Guide: How To Use The Moments Index" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff02"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6669), "Insurtechs have brought innovation across the insurance value chain.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Insurtech Funding.pdf", 1, false, false, "Insurtech Funding Roundup" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff00"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(3944), "Results From Global Banking Platform Deals Survey 2020.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Banking Platform Market.pdf", 1, false, false, "Smaller Banks Are Turning To Fintechs" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff17"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7403), "mCommerce Is A New Mobile Services Economy, Not Mini eCommerce", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mobile Commerce In 2030.pdf", 5, false, false, "Mobile Commerce In 2030" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff16"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7361), "Routine Compliance And Branch Management.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Future Of Work In Banking.pdf", 4, false, false, "The Future Of Work In Banking" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff13"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(7235), "Increasingly Intimate Technologies Spark The Next Mind Shift", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "US Consumers And Technology 2019.pdf", 1, false, false, "US Consumers And Technology, 2019" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff08"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6980), "Retailers Must Distinguish Hot From Hype In Tech Investments.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Retail Technology Investments.pdf", 7, false, false, "The Top Retail Technology Investments In 2019" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff07"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6939), "CES, the world's largest tech show, is about much more than technology.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Five Big Brand Ideas.pdf", 3, false, false, "The CMO's Guide To CES 2019: Five Big Brand Ideas" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff06"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6897), "A Digital Voice Experience Series Report", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Digital Voice Experiences.pdf", 4, false, false, "Digital Voice Experiences" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff05"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6855), "Cost Continues To Be The Biggest Inhibitor To Smart Home Adoption", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Customer Insights Professionals.pdf", 2, false, false, "Smart Speakers Lead The Smart Home Revolution" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff01"), 3, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6574), "Landscape Overview Of 35 Providers.", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Landscape Overview Of 35 Providers.pdf", 1, false, false, "Open Banking Intermediaries, Q2" },
                    { new Guid("11223344-5566-7788-99aa-bbccddeeff04"), 2, new DateTime(2020, 6, 8, 18, 20, 23, 167, DateTimeKind.Utc).AddTicks(6781), "Eighteen Technologies Underpin IoT Solutions", 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tech Tide.pdf", 5, false, false, "Internet Of Things" }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagID", "ReportID" },
                values: new object[,]
                {
                    { 16, new Guid("11223344-5566-7788-99aa-bbccddeeff00") },
                    { 1, new Guid("11223344-5566-7788-99aa-bbccddeeff02") },
                    { 2, new Guid("11223344-5566-7788-99aa-bbccddeeff02") },
                    { 8, new Guid("11223344-5566-7788-99aa-bbccddeeff02") },
                    { 8, new Guid("11223344-5566-7788-99aa-bbccddeeff09") },
                    { 9, new Guid("11223344-5566-7788-99aa-bbccddeeff09") },
                    { 10, new Guid("11223344-5566-7788-99aa-bbccddeeff09") },
                    { 11, new Guid("11223344-5566-7788-99aa-bbccddeeff10") },
                    { 12, new Guid("11223344-5566-7788-99aa-bbccddeeff10") },
                    { 13, new Guid("11223344-5566-7788-99aa-bbccddeeff11") },
                    { 14, new Guid("11223344-5566-7788-99aa-bbccddeeff11") },
                    { 15, new Guid("11223344-5566-7788-99aa-bbccddeeff11") },
                    { 16, new Guid("11223344-5566-7788-99aa-bbccddeeff12") },
                    { 17, new Guid("11223344-5566-7788-99aa-bbccddeeff12") },
                    { 18, new Guid("11223344-5566-7788-99aa-bbccddeeff14") },
                    { 19, new Guid("11223344-5566-7788-99aa-bbccddeeff14") },
                    { 20, new Guid("11223344-5566-7788-99aa-bbccddeeff15") },
                    { 1, new Guid("11223344-5566-7788-99aa-bbccddeeff15") },
                    { 6, new Guid("11223344-5566-7788-99aa-bbccddeeff18") },
                    { 7, new Guid("11223344-5566-7788-99aa-bbccddeeff18") },
                    { 16, new Guid("11223344-5566-7788-99aa-bbccddeeff01") },
                    { 8, new Guid("11223344-5566-7788-99aa-bbccddeeff19") },
                    { 3, new Guid("11223344-5566-7788-99aa-bbccddeeff01") },
                    { 5, new Guid("11223344-5566-7788-99aa-bbccddeeff17") },
                    { 20, new Guid("11223344-5566-7788-99aa-bbccddeeff00") },
                    { 11, new Guid("11223344-5566-7788-99aa-bbccddeeff03") },
                    { 20, new Guid("11223344-5566-7788-99aa-bbccddeeff03") },
                    { 17, new Guid("11223344-5566-7788-99aa-bbccddeeff03") },
                    { 19, new Guid("11223344-5566-7788-99aa-bbccddeeff04") },
                    { 17, new Guid("11223344-5566-7788-99aa-bbccddeeff04") },
                    { 3, new Guid("11223344-5566-7788-99aa-bbccddeeff05") },
                    { 14, new Guid("11223344-5566-7788-99aa-bbccddeeff05") },
                    { 5, new Guid("11223344-5566-7788-99aa-bbccddeeff06") },
                    { 9, new Guid("11223344-5566-7788-99aa-bbccddeeff06") },
                    { 6, new Guid("11223344-5566-7788-99aa-bbccddeeff07") },
                    { 7, new Guid("11223344-5566-7788-99aa-bbccddeeff07") },
                    { 8, new Guid("11223344-5566-7788-99aa-bbccddeeff08") },
                    { 9, new Guid("11223344-5566-7788-99aa-bbccddeeff08") },
                    { 16, new Guid("11223344-5566-7788-99aa-bbccddeeff13") },
                    { 17, new Guid("11223344-5566-7788-99aa-bbccddeeff13") },
                    { 2, new Guid("11223344-5566-7788-99aa-bbccddeeff16") },
                    { 3, new Guid("11223344-5566-7788-99aa-bbccddeeff16") },
                    { 4, new Guid("11223344-5566-7788-99aa-bbccddeeff17") },
                    { 15, new Guid("11223344-5566-7788-99aa-bbccddeeff01") },
                    { 9, new Guid("11223344-5566-7788-99aa-bbccddeeff19") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_DownloadedReports_ReportID",
                table: "DownloadedReports",
                column: "ReportID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AuthorID",
                table: "Reports",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_IndustryID",
                table: "Reports",
                column: "IndustryID");

            migrationBuilder.CreateIndex(
                name: "IX_ReportTags_ReportID",
                table: "ReportTags",
                column: "ReportID");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_IndustryID",
                table: "Subscriptions",
                column: "IndustryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DownloadedReports");

            migrationBuilder.DropTable(
                name: "ReportTags");

            migrationBuilder.DropTable(
                name: "Subscriptions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Industries");
        }
    }
}
