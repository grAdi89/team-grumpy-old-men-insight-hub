﻿using InsightHub.Data.Configurations;
using InsightHub.Data.Seeder;
using InsightHub.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace InsightHub.Data
{
    /// <summary>
    /// A class that sets the Database columns via DbSets, injects 
    /// the model configurations in the OnModel method and initializes the Seeder.
    /// It inherits Microsoft.AspNetCore.Identity.IdentityDbContext.
    /// </summary>
    public class InsightHubContext : IdentityDbContext<User, Role, int>
    {        
        public DbSet<Report> Reports { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ReportTags> ReportTags { get; set; }
        public DbSet<DownloadedReports> DownloadedReports { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }

        public InsightHubContext(DbContextOptions<InsightHubContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ReportConfig());
            modelBuilder.ApplyConfiguration(new IndustryConfig());
            modelBuilder.ApplyConfiguration(new TagConfig());
            modelBuilder.ApplyConfiguration(new ReportTagsConfig());
            modelBuilder.ApplyConfiguration(new SubscriptionConfig());            
            modelBuilder.ApplyConfiguration(new DowloadedReportsConfig());

            modelBuilder.Seeder();

            base.OnModelCreating(modelBuilder);
        }
    }
}
