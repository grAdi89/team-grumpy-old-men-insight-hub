﻿using System;

namespace InsightHub.EmailSender.Services
{
    /// <summary>
    /// A class that implements the configuration properties of EmailConfiguration entity.
    /// </summary>
    public class EmailConfiguration
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
