﻿using System.Threading.Tasks;

namespace InsightHub.EmailSender.Services.Contracts
{
    /// <summary>
    /// Interface for the Email Sender to access the property of the class - SendEmailAsync.
    /// </summary>
    public interface IMyEmailSender
    {
        Task SendEmailAsync(Message message);
    }
}
