﻿using InsightHub.EmailSender.Services;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Services.Contracts;
using InsightHub.Web.Models.UserApiViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    ///API Controller for the Tag service that:
    ///  - provides information for a single user by Id
    ///   - provides information for a list of all users
    ///    - deletes a user
    /// </summary>
    [Authorize(Roles = "Admin")]
    [Route("api/users")]
    [ApiController]
    public class UsersApiController : ControllerBase
    {
        private readonly IUserService _userService;        

        public UsersApiController(IUserService userService)
        {
            _userService = userService;            
        }
        
        [HttpGet]
        [Route("{userId}")]
        public async Task<IActionResult> GetUser(int userId)
        
        {
            try
            {
                var user = await this._userService.GetUserAsync(userId);

                UserApiDetailsViewModel userVM = new UserApiDetailsViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };

                return Ok(userVM);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var users = await this._userService.GetAllUsers();

                var usersVM = users.Select(user => new UserApiDetailsViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName
                });

                return Ok(usersVM);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("delete/{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            try
            {
                var userDeleted = await this._userService.DeleteUserAsync(userId);

                return Accepted("Deleted", userDeleted);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}