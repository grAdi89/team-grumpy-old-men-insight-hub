﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.ReportApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    ///API Controller for the Report service that:
    ///  - creates a new report
    ///   - download reports attached file
    ///    - provides information for a single report by Id
    ///     - provides information for a list of all reports
    ///      - provides the functionality to filter reports by industry, tag and report name
    ///       - provides the functionality to sort reports by number of downloads, date of creation and report name
    ///        - updates a current report
    ///         - deletes a report
    /// </summary>
    [Route("api/reports")]
    [ApiController]
    public class ReportsApiController : ControllerBase
    {

        private readonly IReportService _reportService;
        private readonly IFileTypeProvider _fileTypeProvider;
        public ReportsApiController(IReportService reportService, IFileTypeProvider fileTypeProvider)
        {
            this._reportService = reportService;
            this._fileTypeProvider = fileTypeProvider;
        }

        [Authorize(Roles = "Admin, Author")]
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateReport([FromForm]ReportApiCreateInputModel report)
        {
            try
            {
                if (report.File != null)
                {
                    if (report.File.Length > 25 * 1024 * 1025)
                    {
                        return BadRequest();
                    }
                }

                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var reportDTO = new ReportCreateDTO
                {
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO { UserID = userID },
                    Industry = new IndustryCreateDTO
                    { IndustryID = report.IndustryID },
                    ReportTags = report.Tags.Select(tag => new TagNameDTO { TagName = tag }).ToList(),
                    File = report.File
                };

                var newReport = await this._reportService.CreateReportAsync(reportDTO);

                var newReportView = new ReportApiDetailsViewModel
                {
                    ReportID = newReport.ReportID,
                    ReportName = newReport.ReportName,
                    Description = newReport.Description,
                    Author = reportDTO.Author,
                    Industry = new IndustryApiViewModel 
                    {IndustryID = reportDTO.Industry.IndustryID, IndustryName = reportDTO.Industry.IndustryName },
                    ReportTags = reportDTO.ReportTags.Select(tag => new TagApiViewModel
                    {TagID = tag.TagID, TagName = tag.TagName }).ToList(),
                    FileTitle = reportDTO.FileTitle,                    
                };

                return Created("Post", newReportView);
            }
            catch (Exception)
            {
                return BadRequest($"Report {report.ReportName} already exists!");
            }
        }

        [Authorize]
        [HttpPost]
        [Route("{download}")]
        public async Task<IActionResult> DownloadReport([FromBody] ReportApiDownloadInputModel file)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var report = await this._reportService.GetReportAsync(file.ReportID);
                if (report == null)
                {
                    return BadRequest();
                }

                var memory = await this._reportService.DownloadReportAsync(file.ReportID, userID);

                var ext = _fileTypeProvider.GetFileType(report.FileTitle);

                return File(memory, ext, report.FileTitle);
            }
            catch (Exception)
            {
                return BadRequest($"Report with ID: {file.ReportID} not found!");
            }
        }

        [HttpGet]
        [Route("{reportId}")]
        public async Task<IActionResult> GetReport(Guid reportId)
        {
            try
            {
                var report = await this._reportService.GetReportAsync(reportId);

                ReportApiDetailsViewModel reportViewModels = new ReportApiDetailsViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO 
                    {
                        UserID = report.Author.UserID,
                        FirstName = report.Author.FirstName,
                        LastName = report.Author.LastName
                    },
                    Industry = new IndustryApiViewModel 
                    {IndustryID = report.Industry.IndustryID, IndustryName = report.Industry.IndustryName },
                    ReportTags = report.ReportTags.Select(tagDTO => new TagApiViewModel 
                    {TagID = tagDTO.TagID, TagName = tagDTO.TagName }).ToList(),
                    FileTitle = report.FileTitle,
                    IsFeatured = report.IsFeatured
                };

                return Ok(reportViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllReports()
        {
            try
            {
                var reports = await this._reportService.GetAllReportsAsync();

                var reportViewModels = reports.Select(report => new ReportApiGetAllViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO
                    {
                        UserID = report.Author.UserID,
                        FirstName = report.Author.FirstName,
                        LastName = report.Author.LastName
                    },
                    Industry = new IndustryApiViewModel
                    {
                        IndustryID = report.Industry.IndustryID,
                        IndustryName = report.Industry.IndustryName
                    },
                    ReportTags = report.ReportTags.Select(tag => new TagApiViewModel 
                    {TagID = tag.TagID, TagName = tag.TagName }).ToList(),
                    IsFeatured = report.IsFeatured
                });

                return Ok(reportViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("{filter}/{filterType}/{filterName}")]
        public async Task<IActionResult> FilterReports(string filterType, string filterName)
        {
            try
            {
                var reports = await this._reportService.FilterReportsAsync(filterType, filterName);

                var reportViewModels = reports.Select(report => new ReportApiGetAllViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO
                    {
                        UserID = report.Author.UserID,
                        FirstName = report.Author.FirstName,
                        LastName = report.Author.LastName
                    },
                    Industry = new IndustryApiViewModel
                    {
                        IndustryID = report.Industry.IndustryID,
                        IndustryName = report.Industry.IndustryName
                    },
                    ReportTags = report.ReportTags.Select(tag => new TagApiViewModel 
                    { TagID = tag.TagID, TagName = tag.TagName }).ToList()
                });

                return Ok(reportViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("{sort}/{sortBy}")]
        public async Task<IActionResult> SortReports(string sortBy)
        {
            try
            {
                var reports = await this._reportService.SortReportsAsync(sortBy);

                var reportViewModels = reports.Select(report => new ReportApiGetAllViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO
                    {
                        UserID = report.Author.UserID,
                        FirstName = report.Author.FirstName,
                        LastName = report.Author.LastName
                    },
                    Industry = new IndustryApiViewModel
                    {
                        IndustryID = report.Industry.IndustryID,
                        IndustryName = report.Industry.IndustryName
                    },
                    ReportTags = report.ReportTags.Select(tag => new TagApiViewModel 
                    { TagID = tag.TagID, TagName = tag.TagName }).ToList()
                });

                return Ok(reportViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin, Author")]
        [HttpPut]
        [Route("{reportId}")]
        public async Task<IActionResult> UpdateReport(Guid reportId, [FromForm]ReportApiUpdateInputModel report)
        {
            try
            {
                if (report.File != null)
                {
                    if (report.File.Length > 25 * 1024 * 1025)
                    {
                        return BadRequest();
                    }
                }

                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var oldReport = await this._reportService.GetReportAsync(reportId);

                if (oldReport.Author.UserID != userID && !User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }

                var reportDTO = new ReportCreateDTO
                {
                    ReportID = reportId,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Author = new UserDTO { UserID = userID },
                    Industry = new IndustryCreateDTO
                    { IndustryID = report.IndustryID },
                    ReportTags = report.TagIDs.Select(tag => new TagNameDTO { TagID = tag }).ToList(),
                    File = report.File
                };

                var updatedReport = await this._reportService.UpdateReportAsync(reportDTO);

                ReportApiDetailsViewModel reportViewModel = new ReportApiDetailsViewModel
                {
                    ReportID = updatedReport.ReportID,
                    ReportName = updatedReport.ReportName,
                    Description = updatedReport.Description,
                    Author = new UserDTO
                    {
                        UserID = updatedReport.Author.UserID,
                        FirstName = updatedReport.Author.FirstName,
                        LastName = updatedReport.Author.LastName
                    },
                    Industry = new IndustryApiViewModel
                    {
                        IndustryID = updatedReport.Industry.IndustryID,
                        IndustryName = updatedReport.Industry.IndustryName
                    },
                    ReportTags = updatedReport.ReportTags.Select(tagDTO => new TagApiViewModel
                    {
                        TagID = tagDTO.TagID,
                        TagName = tagDTO.TagName
                    }).ToList(),
                    FileTitle = updatedReport.FileTitle
                };

                return Ok(reportViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin, Author")]
        [HttpDelete]
        [Route("{delete}/{reportId}")]
        public async Task<IActionResult> DeleteReport(Guid reportId)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var report = await this._reportService.GetReportAsync(reportId);

                if (report.Author.UserID != userID && !User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }

                var reports = await this._reportService.DeleteReportAsync(reportId);

                return Ok($"Successfully deleted report wiht ID: {reportId}");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}