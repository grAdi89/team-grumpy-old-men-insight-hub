﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models.ReportApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    ///API Controller for the Tag service that:
    ///  - provides information for a single tag by Id
    ///   - provides information for a list of all tags
    ///    - creates a new tag
    ///     - updates a current tag
    ///      - deletes a tag
    /// </summary>
    [Route("api/tags")]
    [ApiController]
    public class TagsApiController : ControllerBase
    {
        private readonly ITagServices _tagServices;

        public TagsApiController(ITagServices tagServices)
        {
            _tagServices = tagServices;
        }

        [HttpGet]
        [Route("{tagId}")]
        public async Task<IActionResult> GetTag(int tagId)
        {
            try
            {
                var tag = await this._tagServices.GetTagAsync(tagId);

                TagApiDetailsViewModel tagViewModels = new TagApiDetailsViewModel
                {
                    TagID = tag.TagID,
                    TagName = tag.TagName,
                    TagReports = tag.TagReports.Select(reportTag => new ReportNameDescrViewModel
                    {ReportName = reportTag.Report.ReportName, Description = reportTag.Report.Description }).ToList()
                };

                return Ok(tagViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllTags()
        {
            try
            {
                var tagsDTO = await this._tagServices.GetAllTagsAsync();

                var tagsViewModels = tagsDTO.Select(tagDTO => new TagApiViewModel
                {
                    TagID = tagDTO.TagID,
                    TagName = tagDTO.TagName
                });

                return Ok(tagsViewModels);
            }
            catch (Exception)
            {
                return BadRequest($"No tags existing");
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create(TagApiInputModel tag)
        {
            try
            {
                if (tag == null)
                {
                    return NotFound($"You need to enter name to create a tag");
                }

                var newTag = await this._tagServices.CreateTagAsync(tag.TagName);

                return Created("Post", newTag);
            }
            catch (Exception)
            {
                return NotFound($"Tag {tag.TagName} already exists!");
            }
        }

        [HttpPut]
        [Route("")]
        public async Task<IActionResult> UpdateTag([FromBody]TagApiUpdateInputModel tag)
        {
            try
            {
                var tagDTO = new TagDTO
                {
                    TagID = tag.TagID,
                    TagName = tag.TagName
                };

                var updatedtag = await this._tagServices.UpdateTagAsync(tagDTO);

                return Ok(tagDTO);
            }
            catch (Exception)
            {
                return BadRequest($"No tag with {tag.TagName} found");
            }
        }

        [HttpDelete]
        [Route("{tagId}")]
        public async Task<IActionResult> DeleteTag(int tagId)
        {
            try
            {
                bool isTagDeleted = await this._tagServices.DeleteTagAsync(tagId);

                return Accepted("Deleted", isTagDeleted);
            }
            catch (Exception)
            {
                return BadRequest($"Could not delete tag with ID: {tagId}");
            }
        }
    }
}