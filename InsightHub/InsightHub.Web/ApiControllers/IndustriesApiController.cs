﻿using InsightHub.Services.Contracts;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.ReportApiViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    ///API Controller for the Industry service that:
    ///  - provides information for a single industry by Id
    ///   - provides information for a list of all industries
    ///    - creates a new industry
    ///     - updates a current industry
    ///      - deletes a industry
    /// </summary>

    [Route("api/industries")]
    [ApiController]
    public class IndustriesApiController : ControllerBase
    {
        private readonly IIndustryServices _industryServices;

        public IndustriesApiController(IIndustryServices industryServices)
        {
            _industryServices = industryServices;
        }

        [HttpGet]
        [Route("{industryId}")]
        public async Task<IActionResult> GetIndustry(int industryId)
        {
            try
            {
                var industry = await this._industryServices.GetIndustryAsync(industryId);

                IndustryApiDetailsViewModel stylesViewModels = new IndustryApiDetailsViewModel
                {
                    IndustryID = industry.IndustryID,
                    IndustryName = industry.IndustryName,
                    Reports = industry.Reports.Select(report => new ReportNameDescrViewModel
                    {
                        ReportName = report.ReportName,
                        Description = report.Description
                    }).ToList()
                };

                return Ok(stylesViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllIndustries()
        {
            try
            {
                var industriesDTO = await this._industryServices.GetAllIndustriesAsync();

                var industriesViewModels = industriesDTO.Select(industryDTO => new IndustryApiViewModel
                {
                    IndustryID = industryDTO.IndustryID,
                    IndustryName = industryDTO.IndustryName
                });

                return Ok(industriesViewModels);
            }
            catch (Exception)
            {
                return BadRequest($"No industries created");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody]IndustryApiInputModel industry)
        {
            try
            {
                if (industry == null)
                {
                    return BadRequest($"You need to enter name to create an industry");
                }

                var newIndustry = await this._industryServices.CreateIndustryAsync(industry.IndustryName);

                return Created("Post", newIndustry);
            }
            catch (Exception)
            {
                return BadRequest($"Industry {industry.IndustryName} already exists!");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        [Route("")]
        public async Task<IActionResult> UpdateIndustry([FromBody]IndustryApiUpdateInputModel industry)
        {
            try
            {
                var industryDTO = new IndustryCreateDTO
                {
                    IndustryID = industry.IndustryID,
                    IndustryName = industry.IndustryName
                };

                var updatedIndustry = await this._industryServices.UpdateIndustryAsync(industryDTO);

                var industryViewModels = new IndustryApiViewModel
                {
                    IndustryID = updatedIndustry.IndustryID,
                    IndustryName = updatedIndustry.IndustryName
                };

                return Ok(industryViewModels);
        }
            catch (Exception)
            {
                return BadRequest($"No industry with {industry.IndustryName} found");
    }
}

        [Authorize(Roles ="Admin")]
        [HttpDelete]
        [Route("{industryId}")]
        public async Task<IActionResult> DeleteIndustry(int industryId)
        {
            try
            {
                bool isIndustryDeleted = await this._industryServices.DeleteIndustryAsync(industryId);

                return Accepted("Deleted", isIndustryDeleted);
            }
            catch (Exception)
            {
                return BadRequest($"Could not delete industry with ID: {industryId}");
            }
        }
    }
}