﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace InsightHub.Web.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            await _next.Invoke(httpContext);

            switch (httpContext.Response.StatusCode)
            {
                case 404: httpContext.Response.Redirect("/home/PageNotFound");
                    break;
                case 401: httpContext.Response.Redirect("/home/UnauthorizedRequest");
                    break;
                case 400:
                    httpContext.Response.Redirect("/home/VeryBadRequest");
                    break;
                default:
                    break;
            }            
        }
    }
}
