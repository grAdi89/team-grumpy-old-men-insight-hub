﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models.ReportApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    public class TagsController : Controller
    {
        /// <summary>
        ///MVC Controller for the Tag service that:
        ///   - Displays all tags in the index page
        ///    - Displays detailed information for tag in the details page
        ///     - Displays action to create an tag in the create page
        ///      - Displays action to edit a tag in the edit page
        ///       - Displays action to delete a tag in the delete page
        ///          
        /// </summary>
        private readonly ITagServices _tagServices;

        public TagsController(ITagServices tagServices)
        {
            _tagServices = tagServices;
        }

        // GET: Tags
        public async Task<IActionResult> Index()
        {
            var tags = await _tagServices.GetAllTagsAsync();

            var tagViewModels = tags.Select(tag => new TagApiDetailsViewModel
            {
                TagID = tag.TagID,
                TagName = tag.TagName,
                TagReports = tag.TagReports.Select(report => new ReportNameDescrViewModel
                { ReportID = report.ReportID, ReportName = report.Report.ReportName, Description = report.Report.Description }).ToList()
            });

            return View(tagViewModels);
        }

        // GET: Tags/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var tag = await _tagServices.GetTagAsync(id);

                var tagViewModels = new TagApiDetailsViewModel
                {
                    TagID = tag.TagID,
                    TagName = tag.TagName,
                    TagReports = tag.TagReports.Select(report => new ReportNameDescrViewModel
                    { ReportID = report.ReportID, ReportName = report.Report.ReportName, Description = report.Report.Description }).ToList()
                };

                if (tag == null)
                {
                    return NotFound();
                }

                return View(tagViewModels);
            }
            catch(Exception)
            {
                return NotFound();
            }
        }

        // GET: Tags/Create
        [Authorize(Roles = "Author, Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Author, Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TagApiInputModel tag)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _tagServices.CreateTagAsync(tag.TagName);

                    return View();
                }
                return View(tag.TagName);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // GET: Tags/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var tag = await _tagServices.GetTagAsync(id);

                var tagViewModels = new TagApiViewModel
                { TagID = tag.TagID, TagName = tag.TagName };

                if (tag == null)
                {
                    return NotFound();
                }
                return View(tagViewModels);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        // POST: Tags/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TagID,TagName")] TagApiViewModel tag)
        {
            if (id != tag.TagID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var tagDTO = new TagDTO { TagID = tag.TagID, TagName = tag.TagName };

                    await _tagServices.UpdateTagAsync(tagDTO);
                }
                catch (Exception)
                {
                    if (!await TagExists(tag.TagID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tag);
        }

        // GET: Tags/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var tag = await _tagServices.GetTagAsync(id);

                var tagViewModels = new TagApiDetailsViewModel
                {
                    TagID = tag.TagID,
                    TagName = tag.TagName,
                    TagReports = tag.TagReports.Select(report => new ReportNameDescrViewModel
                    { ReportID = report.ReportID, ReportName = report.Report.ReportName, Description = report.Report.Description }).ToList()
                };

                return View(tagViewModels);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        // POST: Tags/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var tag = await _tagServices.DeleteTagAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        private async Task<bool> TagExists(int id)
        {
            try
            {
                var tag = await _tagServices.GetTagAsync(id);

                return true;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
        }
    }
}
