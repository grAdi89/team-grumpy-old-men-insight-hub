﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IReportService _reportService;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IReportService reportService)
        {
            _logger = logger;
            _reportService = reportService;
        }

        public async Task<IActionResult> Index()
        {
            var reportsMostDownloaded = await this._reportService.SortReportsAsync("downloads");
            var reportsMostRecent = await this._reportService.SortReportsAsync("date");
            var reportsFeatured = await this._reportService.GetCurrentlyFeaturedReportsAsync();

            var mostDownloaded = reportsMostDownloaded.Where(x => x.DownloadCount > 0).TakeLast(3).Reverse().ToList();

            var mostRecent = reportsMostRecent.TakeLast(3).Reverse().ToList();

            var indexDownloads = new IndexViewModel
            {
                MostDownloaded = mostDownloaded,
                MostRecent = mostRecent,
                Featured = reportsFeatured.ToList()
            };

            return View(indexDownloads);
        }

        public IActionResult PageNotFound()
        {
            return View();
        }
        public IActionResult VeryBadRequest()
        {
            return View();
        }
        public IActionResult UnauthorizedRequest()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {          
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
