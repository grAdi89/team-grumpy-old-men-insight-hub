﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Models;
using InsightHub.Web.Models.ReportsMVCViewModels;
using InsightHub.Web.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        /// <summary>
        ///MVC Controller for the User service that:
        ///   - Displays all users in the index page
        ///    - Displays detailed information for user in the details page
        ///     - Displays action to delete a report in the delete page
        ///      - Displays functionality to approve/reject pending reports
        ///       - Displays functionality to approve/reject new user accounts
        /// </summary>
        private readonly IUserService _userService;
        private readonly IReportService _reportService;

        public UsersController(IUserService userService, IReportService reportService)
        {
            _userService = userService;
            _reportService = reportService;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var users = await _userService.GetAllUsers();

            var usersVM = users.Select(user => new UserIndexViewModel
            {
                UserID = user.UserID,
                FullName = $"{user.FirstName} {user.LastName}",
                Email = user.Email,
                IsApprovedByAdmin = user.IsApprovedByAdmin,
                HasPendingReports = user.HasPendingReports,
                IsBanned = user.IsBanned
            });

            return View(usersVM);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var user = await _userService.GetUserAsync(id);

                var userVM = new UserIndexViewModel { UserID = user.UserID, FullName = $"{user.FirstName} {user.LastName}", Email = user.Email };

                return View(userVM);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var user = await _userService.GetUserAsync(id);

                var userVM = new UserIndexViewModel
                {
                    UserID = user.UserID,
                    FullName = $"{user.FirstName} {user.LastName}",
                    Email = user.Email,
                    IsApprovedByAdmin = user.IsApprovedByAdmin,
                    HasPendingReports = user.HasPendingReports
                };

                return View(userVM);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var isUserDeleted = await _userService.DeleteUserAsync(id);

            if(!isUserDeleted)
            {
                return NotFound();
            }
           
            return RedirectToAction(nameof(Index));
        }
        
        public async Task<IActionResult> PendingApprovals()
        {
            var usersPendingApproval = await _userService.GetAllPendingApprovalUsers();
            var reportsPendingApproval = await _userService.GetAllPendingApprovalReports();

            var apprVM = new ApprovalsModel
            {
                PendingUsers = usersPendingApproval.Select(user => new UserIndexViewModel
                {
                    UserID = user.UserID,
                    FullName = $"{user.FirstName} {user.LastName}",
                    Email = user.Email

                }).ToList(),
                PendingReports = reportsPendingApproval.Select(rep => new ReportsIndexMVCViewModel
                {
                    ReportID = rep.ReportID,
                    ReportName = rep.ReportName,
                    AuthorID = rep.AuthorID,
                    Author = new UserIndexViewModel { Email = rep.Author.Email }
                }).ToList()
            };           

            return View("Approvals", apprVM);
        }
        
        public async Task<IActionResult> ApprovalConfirmed(int id)
        {
            var isUserApproved = await _userService.ApproveUserAsync(id);

            if(!isUserApproved)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(PendingApprovals));
        }       

        public async Task<IActionResult> ApprovalReportConfirmed(Guid id)
        {
            var isReportApproved = await _userService.ApproveReport(id);

            if (!isReportApproved)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(PendingApprovals));
        }

        public async Task<IActionResult> RejectUserRegistration(int id)
        {
            var isRegistrationRejected = await _userService.DeleteUserAsync(id);

            if(!isRegistrationRejected)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(PendingApprovals));
        }

        public async Task<IActionResult> RejectReport(Guid id)
        {
            var isReportRejected = await _reportService.DeleteReportAsync(id);

            if(!isReportRejected)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(PendingApprovals));
        }
        
        public async Task<IActionResult> Ban(int id)
        {
            try
            {
                var isBanned = await _userService.BanUserAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> Unban(int id)
        {
            try
            {
                var isUnbaned = await _userService.UnbanUserAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }
    }
}
