﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.TagDTOs;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Services.ReportDTOs.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.ReportsMVCViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using InsightHub.Web.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    /// <summary>
    ///MVC Controller for the Report service that:
    ///   - Displays all reports in the index page
    ///    - Displays detailed information for reports in the details page
    ///     - Displays action to create a report in the create page
    ///      - Displays functionality to download reports attached file
    ///       -  Displays all user downloaded reports in MyDownloads page
    ///        -  Displays all user reports in MyReports page
    ///         -  Displays featured reports in the Home page
    ///          - Displays action to edit a report
    ///           - Displays action to delete a report
    /// </summary>
    public class ReportsController : Controller
    {
        private readonly IReportService _reportService;
        private readonly IIndustryServices _industryServices;
        private readonly ITagServices _tagServices;
        private readonly IUserService _userService;
        private readonly IFileTypeProvider _fileTypeProvider;

        public ReportsController(IReportService reportService, IIndustryServices industryServices,
            ITagServices tagServices, IUserService userService, IFileTypeProvider fileTypeProvider)
        {
            _reportService = reportService;
            _industryServices = industryServices;
            _tagServices = tagServices;
            _userService = userService;
            _fileTypeProvider = fileTypeProvider;
        }

        // GET: Reports
        public async Task<IActionResult> Index()
        {
            var reportsDTO = await _reportService.GetAllReportsAsync();

            var reportsView = reportsDTO.Select(report => new ReportsIndexMVCViewModel
            {
                ReportID = report.ReportID,
                ReportName = report.ReportName,
                Description = report.Description,
                Author = new UserIndexViewModel
                { UserID = report.AuthorID, FullName = $"{report.Author.FirstName} {report.Author.LastName}", Email = report.Author.Email},
                AuthorID = report.AuthorID,
                Industry = new IndustryApiViewModel 
                {IndustryID = report.Industry.IndustryID, IndustryName = report.Industry.IndustryName },
                ReportTags = report.ReportTags.Select(tag => new TagApiViewModel 
                {TagID = tag.TagID, TagName = tag.TagName }).ToList(),
                IsFeatured = report.IsFeatured
            });

            return View(reportsView);
        }

        // GET: Reports/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            try
            {
                var reportDTO = await _reportService.GetReportAsync(id);

                var reportViewModel = new ReportDetailsMVCViewModel
                {
                    ReportID = reportDTO.ReportID,
                    ReportName = reportDTO.ReportName,
                    Description = reportDTO.Description,
                    Author = new UserIndexViewModel
                    {
                        UserID = reportDTO.AuthorID,
                        FullName = $"{reportDTO.Author.FirstName} {reportDTO.Author.LastName}",
                        Email = reportDTO.Author.Email
                    },
                    Industry = new IndustryApiViewModel
                    { IndustryID = reportDTO.Industry.IndustryID, IndustryName = reportDTO.Industry.IndustryName },
                    ReportTags = reportDTO.ReportTags.Select(tag => new TagApiViewModel
                    { TagID = tag.TagID, TagName = tag.TagName }).ToList(),
                    FileTitle = reportDTO.FileTitle,
                    CreatedOn = reportDTO.CreatedOn,
                    DownloadCount = reportDTO.DownloadCount
                };

                return View(reportViewModel);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize(Roles ="Author, Admin")]        
        public async Task<IActionResult> Create()
        {           
            ViewData["TagName"] = new SelectList(await _tagServices.GetAllTagsAsync(), "TagName", "TagName");
            ViewData["IndustryName"] = new SelectList(await _industryServices.GetAllIndustriesAsync(), "IndustryID", "IndustryName");
            ReportCreateMVCViewModel newReport = new ReportCreateMVCViewModel();
            return View(newReport);
        }

        [Authorize(Roles = "Author, Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
        [FromForm] ReportCreateMVCViewModel report)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                if (report.File.Length > 25*1024*1024)
                {
                    return RedirectToAction(nameof(Create));
                }

                    var reportDTO = new ReportCreateDTO
                    {
                        ReportID = report.ReportID,
                        ReportName = report.ReportName,
                        Description = report.Description,
                        Author = new UserDTO
                        { UserID = userID },
                        Industry = new IndustryCreateDTO
                        { IndustryID = report.IndustryID },
                        ReportTags = report.Tags.Select(tag => new TagNameDTO
                        { TagName = tag }).ToList(),
                        File = report.File
                    };

                    await _reportService.CreateReportAsync(reportDTO);
                    return RedirectToAction(nameof(Index));
                }

                return RedirectToAction(nameof(Create));
            }
            catch(InvalidOperationException)
            {
                return BadRequest();
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize]
        public async Task<IActionResult> DownloadFile(Guid reportID)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
                var report = await this._reportService.GetReportAsync(reportID);
                if (report == null)
                {
                    return BadRequest();
                }
                var memory = await this._reportService.DownloadReportAsync(reportID, userID);

                var ext = _fileTypeProvider.GetFileType(report.FileTitle);

                return File(memory, ext, report.FileTitle);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Authorize]
        public IActionResult DownloadFileUnauthenticated(Guid reportID)
        {
            return RedirectToAction(nameof(Details), new { id = reportID });            
        }

        [Authorize]
        public async Task<IActionResult> MyDownloads()
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var downloads = await this._reportService.GetMyDownloadsAsync(userID);

                if (downloads == null)
                {
                    return View();
                }

                var userDownloads = downloads.Select(dr => new ReportMyDownloadsMVCViewModel
                {
                    ReportID = dr.ReportID,
                    ReportName = dr.Report.ReportName,
                    Description = dr.Report.Description,
                    FileTitle = dr.Report.FileTitle
                });

                return View(userDownloads);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Authorize]
        public async Task<IActionResult> MyReports()
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var myReports = await this._reportService.GetMyReportsAsync(userID);

                if (myReports == null)
                {
                    return View();
                }

                var userDownloads = myReports.Select(r => new ReportMyDownloadsMVCViewModel
                {
                    ReportID = r.ReportID,
                    ReportName = r.ReportName,
                    Description = r.Description,
                    FileTitle = r.FileTitle
                });

                return View(userDownloads);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DownloadFileForApproval(Guid id)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
                var report = await this._reportService.GetReportForApprovalAsync(id);
                if (report == null)
                {
                    return BadRequest();
                }
                var memory = await this._reportService.DownloadReportAsync(id, userID);

                var ext = _fileTypeProvider.GetFileType(report.FileTitle);

                return File(memory, ext, report.FileTitle);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> Edit(Guid id)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var report = await this._reportService.GetReportAsync(id);

                if (report.Author.UserID != userID && !User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }
                var reportViewModel = new ReportEditMVCViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    FileTitle = report.FileTitle,
                    IndustryID = report.Industry.IndustryID,
                    Industry = new IndustryApiViewModel
                    { IndustryID = report.Industry.IndustryID, IndustryName = report.Industry.IndustryName },
                    Tags = report.ReportTags.Select(tag => tag.TagName).ToList(),
                    AuthorID = report.Author.UserID,
                    Author = report.Author
                };

                ViewData["TagName"] = new SelectList(await _tagServices.GetAllTagsAsync(), "TagName", "TagName", report.ReportTags);
                ViewData["IndustryID"] = new SelectList(await _industryServices.GetAllIndustriesAsync(), "IndustryID", "IndustryName", report.Industry.IndustryID);
                return View(reportViewModel);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Author, Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [FromForm]ReportEditMVCViewModel report)
        {
            if (id != report.ReportID)
            {
                return NotFound();
            }

            if (report.File != null)
            {
                if (report.File.Length > 25 * 1024 * 1025)
                {
                    return RedirectToAction(nameof(Edit));
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var industry = await this._industryServices.GetIndustryAsync(report.IndustryID);

                    var reportCreateDTO = new ReportCreateDTO
                    {
                        ReportID = report.ReportID,
                        ReportName = report.ReportName,
                        Description = report.Description,
                        Industry = new IndustryCreateDTO
                        { IndustryID = industry.IndustryID, IndustryName = industry.IndustryName },
                        ReportTags = report.Tags.Select(tagName => new TagNameDTO { TagName = tagName }).ToList(),
                        Author = new UserDTO { UserID = report.AuthorID },
                        File = report.File,
                        FileTitle = report.FileTitle
                    };

                    await this._reportService.UpdateReportAsync(reportCreateDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ReportExists(report.ReportID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IndustryID"] = new SelectList(await _industryServices.GetAllIndustriesAsync(), "IndustryID", "IndustryName", report.IndustryID);
            return View(report);
        }

        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                var report = await this._reportService.GetReportAsync(id);

                if (report.Author.UserID != userID && !User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }
                
                var reportViewModel = new ReportDetailsMVCViewModel
                {
                    ReportID = report.ReportID,
                    ReportName = report.ReportName,
                    Description = report.Description,
                    Industry = new IndustryApiViewModel
                    { IndustryID = report.Industry.IndustryID, IndustryName = report.Industry.IndustryName },
                    Author = new UserIndexViewModel
                    {
                        UserID = report.AuthorID,
                        FullName = $"{report.Author.FirstName} {report.Author.LastName}",
                        Email = report.Author.Email
                    },
                    ReportTags = report.ReportTags.Select(tag => new TagApiViewModel
                    { TagID = tag.TagID, TagName = tag.TagName }).ToList(),
                    CreatedOn = report.CreatedOn,
                    DownloadCount = report.DownloadCount,
                    FileTitle = report.FileTitle
                };

                return View(reportViewModel);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Author, Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            try
            {
                await _reportService.DeleteReportAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FeatureReport(Guid id)
        {
            var isFeatured = await _reportService.FeatureReportAsync(id);

            if(!isFeatured)
            {
                return BadRequest();
            }
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ReportExists(Guid id)
        {
            var reports = await _reportService.GetAllReportsAsync();

            return reports.Any(e => e.ReportID == id);
        }
    }
}
