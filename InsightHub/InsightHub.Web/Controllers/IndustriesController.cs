﻿using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.IndustryDTOs.DTOs;
using InsightHub.Web.Models.IndustryMVCViewModels;
using InsightHub.Web.Models.ReportsMVCViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    /// <summary>
    ///MVC Controller for the Industry service that:
    ///   - Displays all industries in the index page
    ///    - Displays detailed information for industry in the details page
    ///     - Displays action to create an industry in the create page
    ///      - Displays action to edit a industry in the edit page
    ///       - Displays action to delete a industry in the delete page
    ///        - Displays functionality for users to subscribe/insubscribe to industries
    /// </summary>
    public class IndustriesController : Controller
    {
        private readonly IIndustryServices _industryServices;
        private readonly IUserService _userService;

        public IndustriesController(IIndustryServices industryServices, IUserService userService)
        {
            _industryServices = industryServices;
            _userService = userService;
        }
       
        public async Task<IActionResult> Index()
        {
            var industries = await _industryServices.GetAllIndustriesAsync();
            var industryVM = industries.Select(ind => new IndustryMVCIndexViewModel
            {
                IndustryID = ind.IndustryID,
                IndustryName = ind.IndustryName,
                Subscriptions = ind.Subscriptions.Select(sub => new Subscription
                {
                    IndustryID = sub.IndustryID,
                    UserID = sub.UserID
                }).ToList()
            }).ToList();

            return View(industryVM);
        }
        
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var industry = await _industryServices.GetIndustryAsync(id);

                if (industry == null)
                {
                    return NotFound();
                }
                var industryVM = new IndustryMVCIndexViewModel
                {
                    IndustryID = industry.IndustryID,
                    IndustryName = industry.IndustryName,
                    Reports = industry.Reports.Select(rep => new ReportsIndexMVCViewModel
                    {
                        ReportID = rep.ReportID,
                        ReportName = rep.ReportName
                    }).ToList()
                };

                return View(industryVM);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IndustryMVCCreateViewModel industry)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _industryServices.CreateIndustryAsync(industry.IndustryName);
                    return RedirectToAction(nameof(Index));
                }
                return View(industry);
            }
            catch (InvalidOperationException)
            {
                return View(industry);
            }
        }

        [Authorize(Roles = "Admin")]        
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var industry = await _industryServices.GetIndustryAsync(id);

                var industryVM = new IndustryMVCCreateViewModel
                {
                    IndustryID = industry.IndustryID,
                    IndustryName = industry.IndustryName
                };

                return View(industryVM);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Admin")]        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(IndustryMVCCreateViewModel industry)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var updatedIndustry = new IndustryCreateDTO {IndustryID = industry.IndustryID, IndustryName = industry.IndustryName};

                    await _industryServices.UpdateIndustryAsync(updatedIndustry);
                }
                catch(ArgumentNullException) 
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(industry);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var industry = await _industryServices.GetIndustryAsync(id);

                var industryVM = new IndustryMVCCreateViewModel { IndustryID = industry.IndustryID, IndustryName = industry.IndustryName };

                return View(industryVM);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var industry = await _industryServices.DeleteIndustryAsync(id);                
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public async Task<IActionResult> SubscribeToIndustry(int industryID)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);                

                var isSubscribed = await _userService.SubscribeToIndustry(industryID, userID);

                return RedirectToAction(nameof(Index));
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        public async Task<IActionResult> UnsubscribeFromIndustry(int industryID)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
                var isUnsubscribed = await _userService.UnsubscribeFromIndustry(industryID, userID);

                return RedirectToAction(nameof(Index));
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }        
    }
}

