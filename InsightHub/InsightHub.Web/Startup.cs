using Azure.Storage.Blobs;
using InsightHub.Blob.Services;
using InsightHub.Blob.Services.Contracts;
using InsightHub.Data;
using InsightHub.EmailSender.Services;
using InsightHub.EmailSender.Services.Contracts;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.Providers;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Web.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace InsightHub.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            services.AddSingleton(x => new BlobServiceClient(Configuration.GetValue<string>("AzureBlobsStorageConnectionString")));

            services.AddScoped<IBlobServices, BlobServices>();

            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<IFileTypeProvider, FileTypeProvider>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IIndustryServices, IndustryServices>();
            services.AddScoped<ITagServices, TagServices>();
            services.AddScoped<IReportService, ReportService>();

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<InsightHubContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddDbContext<InsightHubContext>(options =>
                    options
                    .UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "InsightHub API", Version = "v1" });
            });

            var emailConfig = Configuration
                .GetSection("EmailConfiguration")
                .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IMyEmailSender, MyEmailSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InsightHub API" );
            });

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
