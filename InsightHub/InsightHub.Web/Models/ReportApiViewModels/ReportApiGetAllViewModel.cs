﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using System;
using System.Collections.Generic;

namespace InsightHub.Web.Models.ReportApiViewModels
{
    /// <summary>
    /// A class that maps the ReportDTO model to ReportApiGetAllViewModel providing properties for Index view.
    /// </summary>
    public class ReportApiGetAllViewModel
    {
        public ReportApiGetAllViewModel()
        {
            this.ReportTags = new List<TagApiViewModel>();
        }
        public Guid ReportID { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public UserDTO Author { get; set; }
        public IndustryApiViewModel Industry { get; set; }
        public ICollection<TagApiViewModel> ReportTags { get; set; }
        public bool IsFeatured { get; set; }
    }
}
