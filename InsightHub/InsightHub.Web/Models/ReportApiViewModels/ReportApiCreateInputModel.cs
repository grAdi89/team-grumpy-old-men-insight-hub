﻿using InsightHub.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.ReportApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for creating a Report.
    /// </summary>
    public class ReportApiCreateInputModel
    {
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int IndustryID { get; set; }
        public List<string> Tags { get; set; }
        public IFormFile File { get; set; }
    }
}
