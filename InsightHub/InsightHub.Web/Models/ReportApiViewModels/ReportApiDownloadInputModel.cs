﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.ReportApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for downloading a Report.
    /// </summary>
    public class ReportApiDownloadInputModel
    {
        public Guid ReportID { get; set; }
    }
}
