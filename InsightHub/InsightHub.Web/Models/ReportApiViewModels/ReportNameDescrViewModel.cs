﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.ReportApiViewModels
{
    /// <summary>
    /// A class that maps the ReportDTO model to ReportNameDescrViewModel providing less properties for view.
    /// </summary>
    public class ReportNameDescrViewModel
    {
        public Guid ReportID { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
    }
}
