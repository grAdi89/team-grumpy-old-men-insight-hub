﻿using InsightHub.Web.Models.ReportsMVCViewModels;
using InsightHub.Web.Models.UserViewModels;
using System.Collections.Generic;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class providing properties for viewing pending Users and pending Reports.
    /// </summary>
    public class ApprovalsModel
    {
        public ICollection<UserIndexViewModel> PendingUsers { get; set; }
        public ICollection<ReportsIndexMVCViewModel> PendingReports{ get; set; }
    }
}
