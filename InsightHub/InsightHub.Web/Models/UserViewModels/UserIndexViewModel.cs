﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.UserViewModels
{
    /// <summary>
    /// A class providing properties for Index view.
    /// </summary>
    public class UserIndexViewModel
    {
        public int UserID { get; set; }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        public bool IsBanned { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public string Email { get; set; }
        public bool HasPendingReports { get; set; }
    }
}
