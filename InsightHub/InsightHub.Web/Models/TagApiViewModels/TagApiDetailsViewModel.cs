﻿using InsightHub.Web.Models.ReportApiViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.TagApiViewModels
{
    /// <summary>
    /// A class that maps the TagDTO model to TagApiDetailsViewModel providing properties for detailed view.
    /// </summary>
    public class TagApiDetailsViewModel
    {
        public int TagID { get; set; }
        [Display(Name = "Tag")]
        public string TagName { get; set; }
        [Display(Name = "Reports")]
        public ICollection<ReportNameDescrViewModel> TagReports { get; set; }
    }
}
