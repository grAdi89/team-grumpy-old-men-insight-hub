﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.TagApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for update a Tag.
    /// </summary>
    public class TagApiUpdateInputModel
    {
        public int TagID { get; set; }

        [Display(Name = "Tag")]
        public string TagName { get; set; }
    }
}
