﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.TagApiViewModels
{
    /// <summary>
    /// A class that maps the TagDTO model to TagApiViewModel for viewing tag with less details.
    /// </summary>
    public class TagApiViewModel
    {
        public int TagID { get; set; }

        [Display(Name = "Tag")]
        public string TagName { get; set; }
    }
}
