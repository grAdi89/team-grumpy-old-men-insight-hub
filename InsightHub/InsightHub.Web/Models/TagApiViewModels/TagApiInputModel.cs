﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.TagApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for creating a Tag.
    /// </summary>
    public class TagApiInputModel
    {
        [Required(ErrorMessage = "Tag name is required")]
        [MinLength(2, ErrorMessage = "Tag name must be between 2 and 50 symbols")]
        [MaxLength(50, ErrorMessage = "Tag name must be between 2 and 50 symbols")]
        [Display(Name = "Tag")]
        public string TagName { get; set; }
    }
}
