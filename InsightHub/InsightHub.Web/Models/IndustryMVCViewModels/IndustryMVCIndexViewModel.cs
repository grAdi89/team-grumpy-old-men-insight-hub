﻿using InsightHub.Models;
using InsightHub.Web.Models.ReportsMVCViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.IndustryMVCViewModels
{
    /// <summary>
    /// A class that maps the IndustryDTO model to IndustryMVCIndexViewModel providing properties for detailed view.
    /// </summary>
    public class IndustryMVCIndexViewModel
    {
        public int IndustryID { get; set; }
        [Display(Name = "Industry")]
        public string IndustryName { get; set; }
        public ICollection<ReportsIndexMVCViewModel> Reports { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
    }
}
