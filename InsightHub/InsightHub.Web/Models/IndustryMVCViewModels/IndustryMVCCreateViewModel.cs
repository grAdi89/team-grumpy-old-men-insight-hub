﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.IndustryMVCViewModels
{
    /// <summary>
    /// A class containing only the needed properties for creating an Industry.
    /// </summary>
    public class IndustryMVCCreateViewModel
    {
        public int IndustryID { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        [Display(Name = "Industry")]
        public string IndustryName { get; set; }
    }
}
