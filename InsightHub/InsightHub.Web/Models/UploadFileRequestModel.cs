﻿using Microsoft.AspNetCore.Http;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class providing properties for Uploading reports.
    /// </summary>
    public class UploadFileRequestModel
    {
        public int ReportID { get; set; }
        public IFormFile File { get; set; }
    }
}
