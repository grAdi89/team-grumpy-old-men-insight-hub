﻿namespace InsightHub.Web.Models.UserApiViewModels
{
    /// <summary>
    /// A class providing properties for detailed view.
    /// </summary>
    public class UserApiDetailsViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
