﻿namespace InsightHub.Web.Models.IndustryApiViewModels
{
    /// <summary>
    /// A class that maps the IndustryDTO model to IndustryApiViewModel for viewing industry with less details.
    /// </summary>
    public class IndustryApiViewModel
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
    }
}
