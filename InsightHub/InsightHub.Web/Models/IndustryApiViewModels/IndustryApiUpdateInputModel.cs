﻿namespace InsightHub.Web.Models.IndustryApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for updating an Industry.
    /// </summary>
    public class IndustryApiUpdateInputModel
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
    }
}
