﻿using InsightHub.Web.Models.ReportApiViewModels;
using System.Collections.Generic;

namespace InsightHub.Web.Models.IndustryApiViewModels
{
    /// <summary>
    /// A class that maps the IndustryDTO model to IndustryApiDetailsViewModel providing properties for detailed view.
    /// </summary>
    public class IndustryApiDetailsViewModel
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
        public ICollection<ReportNameDescrViewModel> Reports { get; set; }
    }
}
