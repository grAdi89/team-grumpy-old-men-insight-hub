﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.IndustryApiViewModels
{
    /// <summary>
    /// A class containing only the needed properties for creating an Industry.
    /// </summary>
    public class IndustryApiInputModel
    {
        public string IndustryName { get; set; }
    }
}
