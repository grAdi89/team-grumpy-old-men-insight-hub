﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.ReportsMVCViewModels
{
    /// <summary>
    /// A class providing properties for MyDownloads view.
    /// </summary>
    public class ReportMyDownloadsMVCViewModel
    {
        public Guid ReportID { get; set; }

        [Display(Name = "Name")]
        public string ReportName { get; set; }
        public string Description { get; set; }

        [Display(Name = "File Title")]
        public string FileTitle { get; set; }
    }
}
