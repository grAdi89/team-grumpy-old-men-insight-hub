﻿using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using InsightHub.Web.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.ReportsMVCViewModels
{
    /// <summary>
    /// A class providing properties for Index view.
    /// </summary>
    public class ReportsIndexMVCViewModel
    {
        public Guid ReportID { get; set; }

        [Display(Name = "Name")]
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int AuthorID { get; set; }
        public UserIndexViewModel Author { get; set; }
        public int IndustryID { get; set; }
        public IndustryApiViewModel Industry { get; set; }
        public ICollection<TagApiViewModel> ReportTags { get; set; }
        public bool IsFeatured { get; set; }
    }
}
