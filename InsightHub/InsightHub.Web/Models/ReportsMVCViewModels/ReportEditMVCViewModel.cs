﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.ReportsMVCViewModels
{
    /// <summary>
    /// A class containing only the needed properties for updating a Report.
    /// </summary>
    public class ReportEditMVCViewModel
    {
        public Guid ReportID { get; set; }
        
        [Display(Name ="Name")]
        public string ReportName { get; set; }        
        public string Description { get; set; }
        public int AuthorID { get; set; }
        public UserDTO Author { get; set; }
        
        [Display(Name = "Industry")]
        public int IndustryID { get; set; }
        public IndustryApiViewModel Industry { get; set; }        
        public ICollection<string> Tags { get; set; }

        [Display(Name = "File Title")]
        public string FileTitle { get; set; }        
        public IFormFile File { get; set; }
    }
}
