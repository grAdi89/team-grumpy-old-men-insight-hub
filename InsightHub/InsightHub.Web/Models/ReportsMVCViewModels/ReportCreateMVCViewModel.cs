﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models.IndustryApiViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.ReportsMVCViewModels
{
    /// <summary>
    /// A class containing only the needed properties for creating a Report.
    /// </summary>
    public class ReportCreateMVCViewModel
    {
        public Guid ReportID { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        [Display(Name = "Name")]
        public string ReportName { get; set; }
        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }
        public int AuthorID { get; set; }
        public UserDTO Author { get; set; }

        [Required(ErrorMessage = "Industry is required.")]
        [Display(Name = "Industry")]
        public int IndustryID { get; set; }
        public IndustryApiViewModel Industry { get; set; }

        [Required(ErrorMessage = "Tags are required.")]
        public ICollection<string> Tags { get; set; }

        [Required(ErrorMessage = "Attached file is required.")]
        [Display(Name = "Upload your.pdf")]
        public IFormFile File { get; set; }
    }
}
