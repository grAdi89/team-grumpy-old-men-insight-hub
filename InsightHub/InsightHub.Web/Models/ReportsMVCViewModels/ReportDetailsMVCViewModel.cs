﻿using InsightHub.Web.Models.IndustryApiViewModels;
using InsightHub.Web.Models.TagApiViewModels;
using InsightHub.Web.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.ReportsMVCViewModels
{
    /// <summary>
    /// A class that maps the ReportDTO model to ReportApiDetailsViewModel providing properties for detailed view.
    /// </summary>
    public class ReportDetailsMVCViewModel
    {
        public Guid ReportID { get; set; }

        [Display(Name = "Report")]
        public string ReportName { get; set; }
        public string Description { get; set; }
        public UserIndexViewModel Author { get; set; }
        public IndustryApiViewModel Industry { get; set; }

        [Display(Name = "Tags")]
        public ICollection<TagApiViewModel> ReportTags { get; set; }

        [Display(Name = "File Title")]
        public string FileTitle { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Download Count")]
        public int DownloadCount { get; set; }
    }
}
