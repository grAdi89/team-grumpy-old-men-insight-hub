﻿using InsightHub.Services.ReportDTOs.DTOs;
using System.Collections.Generic;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class providing properties for viewing MostDownloaded, MostRecent and Featured Reports on the Home page.
    /// </summary>
    public class IndexViewModel
    {
        public ICollection<ReportDTO> MostDownloaded { get; set; }
        public ICollection<ReportDTO> MostRecent { get; set; }
        public ICollection<ReportDTO> Featured { get; set; }
    }
}
