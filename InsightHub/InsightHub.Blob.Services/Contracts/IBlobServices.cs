﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Blob.Services.Contracts
{
    /// <summary>
    /// Interface for the Blob Service to access properties of the class - DownloadFile, UploadFIle and DeleteFile.
    /// </summary>
    public interface IBlobServices
    {

        Task<MemoryStream> DownloadFileAsync(string name, string oldName);
        Task UploadFileAsync(IFormFile filepond, string newFolder);
        Task DeleteFileAsync(string folder, string fileTitle);
    }
}
