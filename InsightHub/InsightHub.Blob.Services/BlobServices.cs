﻿using Azure.Storage.Blobs;
using InsightHub.Blob.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Blob.Services
{
    /// <summary>
    /// A class that implements upload, download and delete functionalities needed to maintain files in Azure Blob Storage.
    /// </summary>
    public class BlobServices : IBlobServices
    {
        private readonly BlobServiceClient _blobServiceClient;
        const string accountName = "reportsattachments";
        const string key = "YduuuK85R66XL9aq9Xhbrxf2a8RHzbaXNN5/VZpWq/EhqN+nPuZXmdicCyMpDcFjWF03yAuErlVjA9EAAwaGCA==";
        public BlobServices(BlobServiceClient blobServiceClient)
        {
            this._blobServiceClient = blobServiceClient;
        }

        public async Task<MemoryStream> DownloadFileAsync(string name, string oldName)
        {

            var storageAccount = new CloudStorageAccount(
                new StorageCredentials(accountName, key), true);

            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference($"attachments/{name}");

            var blob = container.GetBlockBlobReference($"{oldName}");

            var memory = new MemoryStream();

            await blob.DownloadToStreamAsync(memory);

            memory.Position = 0;

            return memory;
        }

        public async Task UploadFileAsync(IFormFile filepond, string newFolder)
        {
            var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, key), true);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("attachments");
            await container.CreateIfNotExistsAsync();
            await container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });

            CloudBlobDirectory folder = container.GetDirectoryReference(newFolder);

            var blockblob = folder.GetBlockBlobReference(filepond.FileName);

            using (var stream = filepond.OpenReadStream())
            {
                await blockblob.UploadFromStreamAsync(stream);
            }
        }

        public async Task DeleteFileAsync(string folder, string fileTitle)
        {
            var containerClinet = _blobServiceClient.GetBlobContainerClient($"attachments/{folder}");
            var blobClient = containerClinet.GetBlobClient(fileTitle);
            await blobClient.DeleteIfExistsAsync();
        }
    }
}
