## Insight Hub
Web application for providing access to business and tech reports with options for browsing, downloading and creating content.

<br/>
![HomePage](Images/HomePage.png)

## Areas
- Public - accessible without authentication
- Private - available after registration
- Administrative - available for admins only
---
## Sections
- [Public part](#Public-part)
- [Private part](#Private-part)
- [Administrative part](#Administrative-part)

# Public part
 - Home page with full user experience, dynamic view of newest, most downloaded and featured reports.

## Report index
![Reports page](Images/AllReports.png)

 - Browse reports by name, description, industry,authors
 - Dynamic menu for reports details

# Private part

## Client
### Options
- Browse current reports by different criteria
- Download PDF files
- Subscribe by industry for mail subscription
![Report Details](Images/ReportDetails.png)

<br>

## Author
### Options
- Create reports with ability to add new Tags dynamicly
- Modify current reports
- Upload PDF file

![Create report](Images/CreateReport.png)
![Create report](Images/CreateReportAddNewTag.png)

---

# Administrative part

 - Admin panel with options for browsing, approving reports and users. Also editing and deleting industries and tags.

![Pending reports](Images/AdminPanel.png)
![Pending reports](Images/AdminPanelUsers.png)


## Technologies

* ASP.NET Core
* ASP.NET Identity
* Entity Framework Core
* MS SQL Server
* Razor
* AJAX
* JavaScript / jQuery
* HTML
* CSS
* Bootstrap

## Additional information

* Unit tests covering 95% of the business logic
* Used JWT for API authorization
* Used Azure Blob Storage to handle the binary content
* Used Gitflow Workflow throughout the development process
* Implemented Continuous Integration with Gitlab
* Followed the Kanban methodology regarding task management

## API Documentation

* [Swagger](https://localhost:60972/swagger/index.html)

## Team Members

* Svetoslav Savov - [GitLab](https://gitlab.com/svetoslavsavov89)
* Emilian Gradinarov- [GitLab](https://gitlab.com/grAdi89)

## Credits
* Kiril Stanoev
* Anton Gervaziev


